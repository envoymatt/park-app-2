//
//  NearByObject.m
//  Park.com
//
//  Created by Devendra Singh on 7/10/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "NearByObject.h"

@implementation NearByObject
+(NearByObject*)nearByFromDict:(NSDictionary *)dict
{
    NearByObject* nearByObj= [[NearByObject alloc] init];
    
    nearByObj._address = [dict valueForKey:@"Address"];
    nearByObj._approved = [dict valueForKey:@"Approved"];
    nearByObj._city = [dict valueForKey:@"City"];
    nearByObj._coupon = [dict valueForKey:@"Coupon"];
    nearByObj._created = [dict valueForKey:@"Created"];
    nearByObj._currntRate = [dict valueForKey:@"CurrentRate"];
    nearByObj._dailyRatesArry = [dict valueForKey:@"DailyRates"];
    nearByObj._name = [dict valueForKey:@"Name"];
    nearByObj._neigbouhoodId = [dict valueForKey:@"NeighborhoodId"];
    nearByObj._parkType = [dict valueForKey:@"ParkType"];
    nearByObj._parkingType = [dict valueForKey:@"ParkingType"];
    nearByObj._phone = [dict valueForKey:@"Phone"];
    nearByObj._postal = [dict valueForKey:@"Postal"];
    nearByObj._postalPlus = [dict valueForKey:@"PostalPlus"];
    nearByObj._specialsArry = [dict valueForKey:@"Specials"];
    nearByObj._state = [dict valueForKey:@"State"];
    nearByObj._ticktechId = [dict valueForKey:@"TicketechId"];
    nearByObj._type = [dict valueForKey:@"Type"];
    nearByObj._updated = [dict valueForKey:@"Updated"];
    nearByObj._distance = [dict valueForKey:@"Distance"];
    nearByObj._entrancesExitsArry = [dict valueForKey:@"EntrancesExits"];
    nearByObj._favorited = [dict valueForKey:@"Favorited"];
    nearByObj._garageId = [dict valueForKey:@"GarageId"];
    nearByObj._image = [dict valueForKey:@"Image"];
    nearByObj._latitude = [dict valueForKey:@"Latitude"];
    nearByObj._longitute = [dict valueForKey:@"Longitude"];
    nearByObj._monthlyRatesArry = [dict valueForKey:@"MonthlyRates"];
        
    return nearByObj;
}
@end
