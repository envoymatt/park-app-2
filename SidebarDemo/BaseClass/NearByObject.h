//
//  NearByObject.h
//  Park.com
//
//  Created by Devendra Singh on 7/10/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NearByObject : NSObject

@property(nonatomic, retain)NSString* _currentPage;
@property(nonatomic, retain)NSString* _address;
@property(nonatomic, retain)NSString* _approved;
@property(nonatomic, retain)NSString* _city;
@property(nonatomic, retain)NSString* _coupon;
@property(nonatomic, retain)NSString* _created;
@property(nonatomic, retain)NSString* _currntRate;
@property(nonatomic, retain)NSArray* _dailyRatesArry;
@property(nonatomic, retain)NSString* _name;
@property(nonatomic, retain)NSString* _neigbouhoodId;
@property(nonatomic, retain)NSString* _parkType;
@property(nonatomic, retain)NSString* _parkingType;
@property(nonatomic, retain)NSString* _phone;
@property(nonatomic, retain)NSString* _postal;
@property(nonatomic, retain)NSString* _postalPlus;
@property(nonatomic, retain)NSArray* _specialsArry;
@property(nonatomic, retain)NSString* _state;
@property(nonatomic, retain)NSString* _ticktechId;
@property(nonatomic, retain)NSString* _type;
@property(nonatomic, retain)NSString* _updated;
@property(nonatomic, retain)NSString* _distance;
@property(nonatomic, retain)NSArray* _entrancesExitsArry;
@property(nonatomic, retain)NSString* _favorited;
@property(nonatomic, retain)NSString* _garageId;
@property(nonatomic, retain)NSString* _image;
@property(nonatomic, retain)NSString* _latitude;
@property(nonatomic, retain)NSString* _longitute;
@property(nonatomic, retain)NSArray* _monthlyRatesArry;

+(NearByObject*)nearByFromDict:(NSDictionary*)dict;
@end
