//
//  SocketConnect.h
//  ServerConnection
//
//  Created by Harsh Duggal on 11/06/13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
//#import "MBProgressHUD.h"

@class AppDelegate;

@interface ServerConnect : NSObject//<NSStreamDelegate>
{
    id                      _delegate;
    //MBProgressHUD*   _HUD;
    
    
    // data from server
    NSMutableData* _recievedData;
    AppDelegate* _appDelegateObj;

}


@property (nonatomic, retain) id                        _delegate;
@property (nonatomic, retain) NSURLConnection * _connection;

#pragma mark-
#pragma mark- Creating connection with url

-(void)createConnectionRequestToURL:(NSString*)urlStr withJsonString:(NSString *)jsonStringToServer withType:(NSString*)type;
-(void)sendImageToURL:(NSString*)urlStr withImage:(UIImage *)image withImageName:(NSString *)imageName withCoordinates:(NSString*)coord;

-(void)createConnectionRequestToURLWithPost:(NSString*)urlStr withJsonString:(NSString *)jsonStringToServer;

-(void)createConnectionRequestToURLWithHTTPS:(NSString*)urlString withAdditionalParameters:(NSString*)addStr;

@end

@protocol ServerConnectDelegate
- (void) dataLoadRequestFailedWithError:(NSError*)error andMsg:(NSString*)errorMsg;
- (void) dictLoadedFromServer:(NSDictionary*)dict;
- (void) stringLoadedFromServer:(NSString*)str;
- (void) arrayLoadedFromServer:(NSArray*)Array;


//- (void) dataAvailabletoLoad:(SocketConnect*)obj;

@end
