//
//  ParkNeighbourhoodModel.m
//  Park.com
//
//  Created by Mohd Rahib on 28/09/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "ParkNeighbourhoodModel.h"

@implementation ParkNeighbourhoodModel

@synthesize arrayNameFilteration;
@synthesize arrayItemMain;
@synthesize arrayAfterFilteration;
@synthesize arraySelectedBtns;


+ (id)sharedManager {
    static ParkNeighbourhoodModel *parkModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        parkModel = [[self alloc] init];
    });
    return parkModel;
}

- (id)init {
    if (self = [super init]) {
        NSLog(@"here init the class");
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


@end
