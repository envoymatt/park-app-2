//
//  GarageDetailObject.m
//  Park.com
//
//  Created by Devendra Singh on 7/3/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "GarageDetailObject.h"

@implementation GarageDetailObject
+(GarageDetailObject*)garageDetailFromDict:(NSDictionary *)dict
{
    GarageDetailObject* garageDeObj= [[GarageDetailObject alloc] init];
    
    garageDeObj._address = [dict valueForKey:@"Address"];
    garageDeObj._approved = [dict valueForKey:@"Approved"];
    garageDeObj._city = [dict valueForKey:@"City"];
    garageDeObj._coupon = [dict valueForKey:@"Coupon"];
    garageDeObj._created = [dict valueForKey:@"Created"];
    garageDeObj._currntRate = [dict valueForKey:@"CurrentRate"];
    garageDeObj._description = [dict valueForKey:@"Description"];
    garageDeObj._dailyRatesArry = [dict valueForKey:@"DailyRates"];
    garageDeObj._name = [dict valueForKey:@"Name"];
    garageDeObj._neigbouhoodId = [dict valueForKey:@"NeighborhoodId"];
    garageDeObj._parkType = [dict valueForKey:@"ParkType"];
    garageDeObj._parkingType = [dict valueForKey:@"ParkingType"];
    garageDeObj._phone = [dict valueForKey:@"Phone"];
    garageDeObj._postal = [dict valueForKey:@"Postal"];
    garageDeObj._postalPlus = [dict valueForKey:@"PostalPlus"];
    garageDeObj._specialsArry = [dict valueForKey:@"Specials"];
    garageDeObj._state = [dict valueForKey:@"State"];
    garageDeObj._ticktechId = [dict valueForKey:@"TicketechId"];
    garageDeObj._type = [dict valueForKey:@"Type"];
    garageDeObj._updated = [dict valueForKey:@"Updated"];
    garageDeObj._distance = [dict valueForKey:@"Distance"];
    garageDeObj._entrancesExitsArry = [dict valueForKey:@"EntrancesExits"];
    garageDeObj._favorited = [dict valueForKey:@"Favorited"];
    garageDeObj._garageId = [dict valueForKey:@"GarageId"];
    garageDeObj._image = [dict valueForKey:@"Image"];
    garageDeObj._latitude = [dict valueForKey:@"Latitude"];
    garageDeObj._longitute = [dict valueForKey:@"Longitude"];
    garageDeObj._monthlyRatesArry = [dict valueForKey:@"MonthlyRates"];
    
    
    return garageDeObj;
}

@end
