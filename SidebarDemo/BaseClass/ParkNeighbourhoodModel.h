//
//  ParkNeighbourhoodModel.h
//  Park.com
//
//  Created by Mohd Rahib on 28/09/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParkNeighbourhoodModel : NSObject{
    
}


@property (strong, nonatomic) NSArray* arrayNameFilteration;
@property (strong, nonatomic) NSArray* arrayItemMain;
@property (strong, nonatomic) NSArray* arrayAfterFilteration;
@property (strong, nonatomic) NSMutableArray* arraySelectedBtns;
@property (strong, nonatomic) NSString* isReloadData;

+ (id)sharedManager;



@end
