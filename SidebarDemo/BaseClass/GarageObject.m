//
//  GarageObject.m
//  Park.com
//
//  Created by Devendra Singh on 7/2/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "GarageObject.h"

@implementation GarageObject

@synthesize _currentPage;
@synthesize _address;
@synthesize _currntRate;
@synthesize _created;
@synthesize _coupon;
@synthesize _city;
@synthesize _approved;
@synthesize _dailyRatesArry;
@synthesize _name;
@synthesize _neigbouhoodId;
@synthesize _parkingType;
@synthesize _parkType;
@synthesize _phone;
@synthesize _postal;
@synthesize _postalPlus;
@synthesize _specialsArry;
@synthesize _state;
@synthesize _ticktechId;
@synthesize _type;
@synthesize _updated;
@synthesize _entrancesExitsArry;
@synthesize _favorited;
@synthesize _garageId;
@synthesize _image;
@synthesize _latitude;
@synthesize _longitute;
@synthesize _monthlyRatesArry;
@synthesize favKey;


+(GarageObject*)garagesFromDict:(NSDictionary *)dict
{
    GarageObject* garageObj= [[GarageObject alloc] init];
    
    garageObj._address = [dict valueForKey:@"Address"];
    garageObj._approved = [dict valueForKey:@"Approved"];
    garageObj._city = [dict valueForKey:@"City"];
    garageObj._coupon = [dict valueForKey:@"Coupon"];
    garageObj._created = [dict valueForKey:@"Created"];
    garageObj._currntRate = [dict valueForKey:@"CurrentRate"];
    garageObj._dailyRatesArry = [dict valueForKey:@"DailyRates"];
    garageObj._name = [dict valueForKey:@"Name"];
    garageObj._neigbouhoodId = [dict valueForKey:@"NeighborhoodId"];
    garageObj._parkType = [dict valueForKey:@"ParkType"];
    garageObj._parkingType = [dict valueForKey:@"ParkingType"];
    garageObj._phone = [dict valueForKey:@"Phone"];
    garageObj._postal = [dict valueForKey:@"Postal"];
    garageObj._postalPlus = [dict valueForKey:@"PostalPlus"];
    garageObj._specialsArry = [dict valueForKey:@"Specials"];
    garageObj._state = [dict valueForKey:@"State"];
    garageObj._ticktechId = [dict valueForKey:@"TicketechId"];
    garageObj._type = [dict valueForKey:@"Type"];
    garageObj._updated = [dict valueForKey:@"Updated"];
    garageObj._distance = [dict valueForKey:@"Distance"];
    garageObj._entrancesExitsArry = [dict valueForKey:@"EntrancesExits"];
    garageObj._favorited = [dict valueForKey:@"Favorited"];
    garageObj._garageId = [dict valueForKey:@"GarageId"];
    garageObj._image = [dict valueForKey:@"Image"];
    garageObj._latitude = [dict valueForKey:@"Latitude"];
    garageObj._longitute = [dict valueForKey:@"Longitude"];
    garageObj._monthlyRatesArry = [dict valueForKey:@"MonthlyRates"];
    garageObj.favKey = NO;
    
    return garageObj;
}


@end
