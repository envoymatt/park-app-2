 //
//  SocketConnect.m
//  ServerConnection
//
//  Created by Harsh Duggal on 11/06/13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ServerConnect.h"
#import "AppDelegate.h"
#import "Utility.h"



@implementation ServerConnect


@synthesize _delegate;
@synthesize _connection;


#pragma mark- establishing connection b/w server and client

#pragma mark -Connection Delegates-
#pragma mark- creating connection with url
-(void)createConnectionRequestToURL:(NSString*)urlStr withJsonString:(NSString *)jsonStringToServer withType:(NSString*)type
{
    _appDelegateObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSLog(@"createConnectionRequestToURL:%@",urlStr);
    
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString *getString = [NSString stringWithFormat:@"%@",jsonStringToServer];
    
    NSData *getData = [getString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:NO];
      
    NSString *getLength = [NSString stringWithFormat:@"%lu",(unsigned long)[getData length]];
    
    
    NSMutableURLRequest *request= [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                         timeoutInterval:60.0];
    
    
    [request setURL:[NSURL URLWithString:urlStr]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@ %@",@"parkAppDefault",@"r2ipark2014",@"rpp=10&lat=40.8&lon=-73.92&radius=1.5"];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    if([type isEqualToString:@"POST"])
    {
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
    else
    {
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    }


    [request setValue:getLength forHTTPHeaderField:@"Content-Length"];

    [request setHTTPBody:getData];
        
    
   _connection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        
    if (_connection) {
               _recievedData = [NSMutableData data] ;
    }
    else {
        NSLog(@"Connection not created");
    }
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}

-(void)createConnectionRequestToURLWithHTTPS:(NSString*)urlString withAdditionalParameters:(NSString*)addStr
{
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    
    NSURL *url = [NSURL URLWithString:urlString];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:60];
    
    //(void)[NSURLConnection connectionWithRequest:request delegate:self];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@",@"parkAppDefault",@"r2ipark2014"];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:addStr forHTTPHeaderField:@"Header"];

    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;

    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
    NSError* error;

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"URL is %@",urlString);
    
    id object = [NSJSONSerialization
                    JSONObjectWithData:response
                    options:kNilOptions
                     error:&error];
    
    //NSLog(@"Object %@",object);
    
    if (error)
    {
        NSLog(@"Error in rx data:%@",[error description]);
        NSString * temp = [[NSString alloc]initWithData:response encoding:NSASCIIStringEncoding];
        //NSString * temp = [[NSString alloc]initWithData:_recievedData encoding:NSUTF8StringEncoding];
        NSLog(@"Error String :%@",temp);
    }
    
    // oject of class string
    if([object isKindOfClass:[NSString class]] == YES) {
        NSLog(@"String rx from server");
        [_delegate stringLoadedFromServer:object];
    }
    // object of class dictionary
    else if ([object isKindOfClass:[NSDictionary class]] == YES) {
        NSLog(@"Dictionary rx from server:");
        [_delegate dictLoadedFromServer:object];
    }
    // object of class array
    else if ([object isKindOfClass:[NSArray class]] == YES) {
        NSLog(@"Array rx from server");
        [_delegate arrayLoadedFromServer:object];
    }
    //NSLog(@"Data %@ /n and Object %@",response,object);

}

-(void)createConnectionRequestToURLWithPost:(NSString*)urlStr withJsonString:(NSString *)jsonStringToServer
{
    _appDelegateObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSLog(@"createConnectionRequestToURL:%@",urlStr);
    
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *getString = [NSString stringWithFormat:@"%@",jsonStringToServer];
    
    NSData *getData = [getString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:NO];
    
    NSString *getLength = [NSString stringWithFormat:@"%lu",(unsigned long)[getData length]];
    
    
    NSMutableURLRequest *request= [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                      timeoutInterval:60.0];
    
    [request setURL:[NSURL URLWithString:urlStr]];
    
        [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        //[request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:getLength forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:getData];
    
    
    _connection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (_connection) {
        _recievedData = [NSMutableData data] ;
    }
    else {
        NSLog(@"Connection not created");
    }
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
}


-(void)sendImageToURL:(NSString*)urlStr withImage:(UIImage *)image withImageName:(NSString *)imageName withCoordinates:(NSString*)coord
{
    NSLog(@"Image name is %@",imageName);
    NSData *imageData = UIImageJPEGRepresentation(image,1);
    NSString *urlString = [NSString stringWithFormat:@"%@",urlStr];
    // setting up the request object now
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = [[NSString alloc]init];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"file\"; filename=\"test.png\"rn" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/%@.jpg\r\n\r\n",imageName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[coord dataUsingEncoding:NSUTF8StringEncoding]];

    [request setHTTPBody:body];
    
    _connection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (_connection) {
        _recievedData = [NSMutableData data] ;
    }
    else {
        NSLog(@"Connection not created");
    }
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    
    
}
#pragma mark- Connection Delegates-

//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//}

//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.
//                                             serverTrust] forAuthenticationChallenge:challenge];
//    
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
//}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response // This method is called when the server has determined that it has enough information to create the NSURLResponse. It can be called multiple times, for example in the case of a redirect, so each time we reset the data.
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSString * tempResponseStatus =  @"";
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    NSLog(@"ConnectionDidReceiveResponse %ld", (long)[httpResponse statusCode]);
    if ([httpResponse statusCode] >= 400) {
        tempResponseStatus =[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]];
        NSLog(@"remote url returned error %ld %@",(long)[httpResponse statusCode],tempResponseStatus);
    }

    // receivedData is an instance variable declared elsewhere.
    [_recievedData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSLog(@"connection:didReceiveData");
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [_recievedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    // inform the user
    NSString * tempErrorMsg = [NSString stringWithFormat:@"Error - %@ %@",[error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];

    NSLog(@"Connection failed!%@",tempErrorMsg);
    [_delegate dataLoadRequestFailedWithError:error andMsg:tempErrorMsg];

//    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Connection failed!!" message:@" The Internet connection appears to be offline." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [alert show];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"Succeeded! Received %d bytes of data",[_recievedData length]);

    NSError* error;
    id object = [NSJSONSerialization
                 JSONObjectWithData:_recievedData
                 options:kNilOptions
                 error:&error];
    if (error){
        NSLog(@"Error in rx data:%@",[error description]);
        NSString * temp = [[NSString alloc]initWithData:_recievedData encoding:NSASCIIStringEncoding];
        //NSString * temp = [[NSString alloc]initWithData:_recievedData encoding:NSUTF8StringEncoding];
        NSLog(@"Error String :%@",temp);
        [_delegate dataLoadRequestFailedWithError:error andMsg:nil];
        
        return;
    }
    // oject of class string
    if([object isKindOfClass:[NSString class]] == YES) {
        NSLog(@"String rx from server");
        [_delegate stringLoadedFromServer:object];
    }
    // object of class dictionary
    else if ([object isKindOfClass:[NSDictionary class]] == YES) {
        NSLog(@"Dictionary rx from server:");
        [_delegate dictLoadedFromServer:object];
    }
    // object of class array
    else if ([object isKindOfClass:[NSArray class]] == YES) {
        NSLog(@"Array rx from server");
        [_delegate arrayLoadedFromServer:object];
    }
}





//// Authentication delegate methods
//- (BOOL)connection:(NSURLConnection *)connection
//canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
//{
////#pragma mark unused (connection)
////    assert(connection == connect);
////    assert(protectionSpace != nil);
//    
//    BOOL retVal;
//    retVal = [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//    
//    return YES;
//}
//
////connection:willSendRequestForAuthenticationChallenge
//
//- (void)connection:(NSURLConnection *)connection
//didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//    // Tries to connect 3 times, after the 3 attempts it cancels.
//    if ([challenge previousFailureCount] < 3) {
//        // Create credentials from the user name and password and save them, for this
//        // specific authentication challenge.
//        NSURLCredential *credentials = [NSURLCredential credentialWithUser:@"parkAppDefault" password:@"r2ipark2014" persistence:NSURLCredentialPersistenceForSession];
//        //      [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
//        // Handle the credentials according the state of the challenge.
//        if (challenge == nil) {
//            [[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
//        }
//        else {
//            [challenge.sender useCredential:credentials forAuthenticationChallenge:challenge];
//        }
//    }
//    else {
//        [[challenge sender] cancelAuthenticationChallenge:challenge];
//        NSLog(@"Wrong username or password");
//    }
//}
//
//// Save credentials for further use.
//- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection
//{
//    return YES;
//}





@end
