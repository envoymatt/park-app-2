//
//  SettingsTableViewCell.h
//  Park.com
//
//  Created by Mohd. Rahib on 25/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView* image_Icon;
@property (strong, nonatomic) IBOutlet UIButton* button_Connect;
@property (strong, nonatomic) IBOutlet UILabel* label_Title;

@end
