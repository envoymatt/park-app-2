//
//  SettingsTableViewCell.m
//  Park.com
//
//  Created by Mohd. Rahib on 25/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "SettingsTableViewCell.h"

@implementation SettingsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
