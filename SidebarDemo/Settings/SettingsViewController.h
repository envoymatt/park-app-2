//
//  SettingsViewController.h
//  Park.com
//
//  Created by Mohd. Rahib on 25/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Utility.h"
#import "SWRevealViewController.h"
#import "SettingsTableViewCell.h"


@interface SettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
