//
//  SettingsViewController.m
//  Park.com
//
//  Created by Mohd. Rahib on 25/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController (){
    NSString* cellIdentifier;
    AppDelegate* appDelObj;
    NSArray* _menuItems;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _menuItems = @[@"Park Mobile",@"Passport Parking",@"Best Parking",@"Spot Hero",@"ParkWhiz",@"OpenTable",@"Yelp",@"Tripit",@"Moovit"];
    appDelObj = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    appDelObj.recentVisitedArr = (NSMutableArray*)[[appDelObj.recentVisitedArr reverseObjectEnumerator] allObjects];
}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    //[menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    
    
//    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
//    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
//    [searchButton setImage:[UIImage imageNamed:@"searchimage.png"] forState:UIControlStateNormal];
//    
//    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [navRightView addSubview:searchButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navRightView addSubview:titleSepratorImg];
    
//    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
//    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Settings";
    self.navigationItem.titleView = label;
    
}

#pragma mark TableView delegates & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _menuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellId = @"settingCell";
    SettingsTableViewCell* cell = (SettingsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    cell.label_Title.text = _menuItems[indexPath.row];
    [cell.button_Connect setTag:indexPath.row];
    [cell.button_Connect addTarget:self action:@selector(connectBtnTaped:) forControlEvents:UIControlEventTouchUpInside];
    cell.image_Icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@",[_menuItems objectAtIndex:indexPath.row],@".png"]];
    return cell;
    
}

-(void)connectBtnTaped:(UIButton *)sender{
    [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ selected",[_menuItems objectAtIndex:sender.tag]] message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
