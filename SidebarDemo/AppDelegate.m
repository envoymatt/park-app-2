//
//  AppDelegate.m
//  SidebarDemo
//
//  Created by Simon on 28/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize recentVisitedArr;
@synthesize favGarageArr;
@synthesize locationManager;
@synthesize isHitAPI;
@synthesize searchString;
//@synthesize longitute;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    sleep(1);
    isHitAPI = NO;
    [Appsee start:@"cd99e1138e044407bc89fc34072896de"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
    recentVisitedArr = [[NSMutableArray alloc]init];
    favGarageArr =     [[NSMutableArray alloc]init];
    
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [locationManager startUpdatingLocation];
    
//    latitute = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
//    NSLog(@"%f",[latitute floatValue]);
//
//    longitute = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    return YES;
}

//#pragma mark - CLLocationManagerDelegate
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    NSLog(@"didFailWithError: %@", error);
//    [ [[UIAlertView alloc]
//       initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//    
//}
//-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
//    
//    NSLog(@"%@",[NSString stringWithFormat:@"%f,%f",manager.location.coordinate.latitude,manager.location.coordinate.longitude]);
//    latitute = [NSString stringWithFormat:@"%f",manager.location.coordinate.latitude];
//    longitute = [NSString stringWithFormat:@"%f",manager.location.coordinate.longitude];
////    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
////    [defaults setDouble:manager.location.coordinate.latitude forKey:@"Latitute"];
////    [defaults setDouble:manager.location.coordinate.longitude forKey:@"Longitute"];
////    [defaults synchronize];
//    [manager stopUpdatingLocation];
//    
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
