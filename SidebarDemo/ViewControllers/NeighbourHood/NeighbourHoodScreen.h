//
//  NeighbourHoodScreen.h
//  Park.com
//
//  Created by Devendra Singh on 7/3/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkNeighbourhoodModel.h"

@interface NeighbourHoodScreen : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
