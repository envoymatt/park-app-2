//
//  NeighbourHoodScreen.m
//  Park.com
//
//  Created by Devendra Singh on 7/3/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "NeighbourHoodScreen.h"
#import "Utility.h"

@interface NeighbourHoodScreen (){
    NSArray* arrayNeighVal;
    NSMutableArray* arrayCheckBtns;
    NSMutableArray* arrayFilteredFinal;
    ParkNeighbourhoodModel* parkModel;
    NSInteger cnt;
}
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)updateBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNeighVal;

@end

@implementation NeighbourHoodScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    parkModel = [ParkNeighbourhoodModel sharedManager];
    arrayNeighVal = [[NSArray alloc] initWithArray:parkModel.arrayNameFilteration];
    arrayCheckBtns = [[NSMutableArray alloc] init];
    arrayFilteredFinal = [[NSMutableArray alloc] init];
    NSLog(@"%@",parkModel.arraySelectedBtns);
    cnt = 0;
    [self.tableViewNeighVal setFrame:CGRectMake(self.tableViewNeighVal.frame.origin.x, self.tableViewNeighVal.frame.origin.y, self.tableViewNeighVal.frame.size.width, ([arrayNeighVal count]+1)*50)];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 55, 30)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 55, 30)];
    //[menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    [menuButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [menuButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(220, 0, 50, 30)];
    //[navRightView setBackgroundColor:[UIColor redColor]];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 50, 30)];
    [searchButton setTitle:@"APPLY" forState:UIControlStateNormal];
    [searchButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    //[searchButton setBackgroundColor:[UIColor blueColor]];
    //[searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(applyBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:searchButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    //[self navigationViewTitle];
    //[self navigationSearchBar];
    
}

-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)applyBtnClicked:(id)sender
{
    parkModel.arraySelectedBtns = arrayCheckBtns;
    [arrayFilteredFinal removeAllObjects];
    for (NSInteger index = 0; index < [arrayCheckBtns count]-1; index++) {
        UIButton* sender = (UIButton*) arrayCheckBtns[index+1];
        if ([sender isSelected]) {
            for (NSInteger indexTwo = 0; indexTwo < [parkModel.arrayItemMain count]; indexTwo++) {
                if ([arrayNeighVal[index] isEqualToString:[parkModel.arrayItemMain[indexTwo] valueForKey:@"NeighborhoodName"]]) {
                    [arrayFilteredFinal addObject:parkModel.arrayItemMain[indexTwo]];
                }
                
            }
            
        }
    }
    
    NSLog(@"%@",arrayFilteredFinal);
    parkModel.arrayAfterFilteration = arrayFilteredFinal;
    [self.navigationController popViewControllerAnimated:YES];

    
    
}
-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Neighbourhood";
    self.navigationItem.titleView = label;
    
}

-(void)navigationSearchBar
{
    CGRect frame = CGRectMake(35,0,175,29);
    UIView* searchView = [[UIView alloc] initWithFrame:frame];
    UITextField* searchTxtFled = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 175, 29)];
    [searchTxtFled setBackground:[UIImage imageNamed:@"header-search.png"]];
    [searchTxtFled setEnabled:NO];
    //[searchTxtFled setPlaceholder:@"New York, NY"];
    [searchView addSubview:searchTxtFled];
    self.navigationItem.titleView = searchView;
    
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - UIButton Actions
- (IBAction)cancelBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)updateBtnClicked:(id)sender {
}


#pragma mark TableView Delegate and DataSorce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayNeighVal count]+1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"cellID";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIButton* checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkBtn setFrame:CGRectMake(260, 10, 30, 30)];
    [checkBtn setTag:indexPath.row];
    [checkBtn setImage:[UIImage imageNamed:@"check_button"] forState:UIControlStateNormal];
    [checkBtn setImage:[UIImage imageNamed:@"check_button_pressed"] forState:UIControlStateSelected];
    [checkBtn addTarget:self action:@selector(checkTaped:) forControlEvents:UIControlEventTouchUpInside];
    //indexPath.row > 0 ? [arrayCheckBtns addObject:checkBtn] : NSLog(@"this is 0th index");
    [arrayCheckBtns addObject:checkBtn];
    [self checkButtonState:indexPath.row];
    [cell addSubview:checkBtn];
    if (indexPath.row == 0)
        cell.textLabel.text = @"Select All";
    else
        cell.textLabel.text = [arrayNeighVal objectAtIndex:(indexPath.row)-1];
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        if ([parkModel.arraySelectedBtns count] > 0) {
            [parkModel.arraySelectedBtns count] == cnt ? cnt-- : NSLog(@"no object is selected");
        }
    }
}

-(void)checkTaped:(UIButton*)sender{
    if ([sender isSelected]){
        sender.selected = NO;
        if (sender.tag == 0) {
            for (UIButton* btns in arrayCheckBtns)
                [btns setSelected:NO];
            cnt = 0;
        }
        else
            cnt--;
    }
    else{
        sender.selected = YES;
        if (sender.tag == 0) {
            for (UIButton* btns in arrayCheckBtns)
                [btns setSelected:YES];
            cnt = arrayCheckBtns.count-1;
        }
        else
            cnt++;
    }
    NSLog(@"%ld",(long)cnt);
    if (cnt > arrayCheckBtns.count-2){
        [arrayCheckBtns[0] setSelected:YES];
    }
    else
        [arrayCheckBtns[0] setSelected:NO];
    
}

-(void)checkButtonState:(NSInteger)btnIndex{
    if ([parkModel.arraySelectedBtns count] > btnIndex) {
        if ([parkModel.arraySelectedBtns[btnIndex] isSelected]) {
            UIButton* btn = arrayCheckBtns[btnIndex];
            btn.selected = YES;
            cnt = cnt+1;
        }
    }
}


@end
