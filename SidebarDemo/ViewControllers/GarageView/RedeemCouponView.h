//
//  RedeemCouponView.h
//  Park.com
//
//  Created by Mohd. Rahib on 26/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemCouponView : UIView

@property (strong, nonatomic) IBOutlet UIButton* btn_Redeem;
@property (strong, nonatomic) IBOutlet UIView* couponViewObj;
@property (strong, nonatomic) IBOutlet UIView* pickerViewObj;


@end
