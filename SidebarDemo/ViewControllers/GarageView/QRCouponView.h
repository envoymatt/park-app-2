//
//  QRCouponView.h
//  Park.com
//
//  Created by Mohd. Rahib on 26/08/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCouponView : UIView

@property (strong, nonatomic) IBOutlet UIButton* btn_QR;

@end
