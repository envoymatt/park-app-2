//
//  GarageView.h
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "ServerConnect.h"
#import "GarageObject.h"
#import "RedeemCouponView.h"


@interface GarageView : UIViewController<CLLocationManagerDelegate>{
}

- (void)garageDetailId:(GarageObject*)garObj;
   // Garage Detail Id send from Park Map View.
@end
