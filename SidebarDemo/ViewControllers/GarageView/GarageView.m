//
//  GarageView.m
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "GarageView.h"
#import "Utility.h"
#import "GarageDetailObject.h"
#import "NavigationDecisionScreen.h"
#import "QRCouponView.h"

@interface GarageView ()
{
    GarageObject* garageObj;
    NSMutableArray* itemsArry;
    NSString* currentAddress;
    NSString* longit;
    NSString* latitu;
    
    CLLocation* _location;
    MKAnnotationView *annView;
    UIButton *pinButton;
    int buttonIndex;
    UIView* myview;
    UIImageView* mapPopView;
    UILabel* userLocLbl;
    UIButton* closeBtn;
    RedeemCouponView* couponView;
    QRCouponView* qrViewObj;

    
    //CLLocationManager* locationManager;
    //CLGeocoder *geocoder;
    //CLPlacemark *placemark;
    
    IBOutlet UILabel* dollarLbl;
    IBOutlet UIButton* favBtn;

    
}
@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (weak, nonatomic) IBOutlet UILabel *_garageNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *_garageAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *_cityStatePostalLbl;
@property (weak, nonatomic) IBOutlet UILabel *_phoneNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *_typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *_currentPriceLbl;
@property (weak, nonatomic) IBOutlet MKMapView *_mkMapView;
@property (weak, nonatomic) IBOutlet UILabel *_descriptionLbl;
@property (weak, nonatomic) IBOutlet UIView *_whatsAroundVw;
@property (weak, nonatomic) IBOutlet UIView *_descriptionView;
@property (weak, nonatomic) IBOutlet UIView *_amentiesView;


- (IBAction)favoriteBtnClicked:(id)sender;
- (IBAction)navigationBtnClicked:(id)sender;
- (IBAction)redeemCouponTaped:(id)sender;
- (IBAction)QRCouponTaped:(id)sender;


@end

@implementation GarageView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    itemsArry = [[NSMutableArray alloc] init];
    
    [self._scrollView setContentSize:CGSizeMake(320, 2100)];
    [self apiCallGarageDetails:garageObj._garageId];
    
//    locationManager = [[CLLocationManager alloc] init];
//    geocoder = [[CLGeocoder alloc] init];
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [locationManager startUpdatingLocation];

    
    

}

-(void)showDetailsOfGarage{
    
    if(garageObj.favKey){
        [favBtn setImage:[UIImage imageNamed:@"btn-favorite2-1"] forState:UIControlStateNormal];
        favBtn.selected = YES;
    }
    else{
        [favBtn setImage:[UIImage imageNamed:@"btn-favorite2-0"] forState:UIControlStateNormal];
        favBtn.selected = NO;
    }

    
    
    for (int i=0; i<[itemsArry count]; i++) {
        
        GarageDetailObject* gaDetObj = (GarageDetailObject*)[itemsArry objectAtIndex:i];
        dollarLbl.text = [NSString stringWithFormat:@"$%@",gaDetObj._currntRate];
        
        if([garageObj._garageId intValue] == [gaDetObj._garageId intValue]){
            if (gaDetObj._name != (id)[NSNull null]) {
                self._garageNameLbl.text = gaDetObj._name;
            }
            if (gaDetObj._address != (id)[NSNull null]) {
                self._garageAddressLbl.text = gaDetObj._address;
            }
            
            self._cityStatePostalLbl.text = [NSString stringWithFormat:@"%@, %@, %@",gaDetObj._city,gaDetObj._state,gaDetObj._postal];
            
            if(gaDetObj._description != (id)[NSNull null]){
                self._descriptionLbl.text = gaDetObj._description;

                self._descriptionView.hidden = NO;
                [self setNewFrameForLabel:self._descriptionLbl];
                [self._descriptionView setFrame:CGRectMake(self._descriptionView.frame.origin.x, self._descriptionView.frame.origin.y, self._descriptionView.frame.size.width,self._descriptionLbl.frame.origin.y+ self._descriptionLbl.frame.size.height+5)];
                [self._amentiesView setFrame:CGRectMake(self._amentiesView.frame.origin.x, self._descriptionView.frame.origin.y+self._descriptionView.frame.size.height, self._amentiesView.frame.size.width, self._amentiesView.frame.size.height)];

            }
            else{
                self._descriptionView.hidden = YES;
                [self._amentiesView setFrame: CGRectMake(self._amentiesView.frame.origin.x, self._amentiesView.frame.origin.y-self._descriptionView.frame.size.height, self._amentiesView.frame.size.width, self._amentiesView.frame.size.height)];
            }
            if(gaDetObj._phone != (id)[NSNull null]){
                NSMutableString *stringts = [NSMutableString stringWithString:gaDetObj._phone];
                [stringts insertString:@"-" atIndex:3];
                [stringts insertString:@"-" atIndex:7];
                self._phoneNoLbl.text = stringts;
            }
            
            if (gaDetObj._type != (id)[NSNull null]) {
                self._typeLbl.text = [NSString stringWithFormat:@"Type: %@",gaDetObj._type];
            }
            
            if (gaDetObj._currntRate != (id)[NSNull null]) {
                self._currentPriceLbl.text = [NSString stringWithFormat:@"$%@",gaDetObj._currntRate];
            }
            
            return;
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(headerBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    
    
    
    //    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    //    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    //    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    //
    //    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [navRightView addSubview:searchButton];
    //
    //    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //    //[navRightView addSubview:titleSepratorImg];
    //
    //    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    //    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = garageObj._name;
    self.navigationItem.titleView = label;
    
}


#pragma mark - Garage Details
- (void)garageDetailId:(GarageObject*)obj{
    NSLog(@"Garage Details Id %@",obj._garageId);
    garageObj = obj;
}

#pragma mark ApiCall
-(void)apiCallGarageDetails:(NSString*)gId
{
    [Utility showLoadingIndicator:@"" withView:self.view];
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    [servConnectObj createConnectionRequestToURLWithHTTPS:[NSString stringWithFormat:@"https://api.park.com/Garages/%@",gId] withAdditionalParameters:@""];
}

#pragma mark - MapView Delegate
-(void)mapViewDetails
{
    NSMutableArray* arr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<[itemsArry count];i++)
    {
        GarageDetailObject* garDetObje = (GarageDetailObject*)[itemsArry objectAtIndex:i];
        _location = [[CLLocation alloc]initWithLatitude:[garDetObje._latitude floatValue] longitude:[garDetObje._longitute floatValue]];
        
        [arr addObject:_location];
    }
    [self annotationPinDetails];
    
}

-(void)annotationPinDetails
{
    for(int i=0;i<[itemsArry count];i++)
    {
        GarageDetailObject* garDeObjs = (GarageDetailObject*)[itemsArry objectAtIndex:i];
        
        NSLog(@"Add %@ \n Lat %@ \n Long %@ \n CRate %@",garDeObjs._address,garDeObjs._latitude,garDeObjs._longitute,garDeObjs._currntRate);
        
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake(0, 0);
        
        if(garDeObjs._latitude != nil && garDeObjs._latitude != (id)[NSNull null])
        {
            annotationCoord.latitude =  [garDeObjs._latitude floatValue];
            annotationPoint.coordinate = annotationCoord;
            
        }
        if(garDeObjs._longitute != nil && garDeObjs._longitute != (id)[NSNull null])
        {
            annotationCoord.longitude = [garDeObjs._longitute floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        [self._mkMapView addAnnotation:annotationPoint];
        
        currentAddress = [NSString stringWithFormat:@"%@ %@ %@",garDeObjs._address,garDeObjs._city,garDeObjs._state];
        latitu = garDeObjs._latitude;
        longit = garDeObjs._longitute;
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self._mkMapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [self._mkMapView setVisibleMapRect:zoomRect animated:YES];
}

- (void) handlePinButtonTap:(UITapGestureRecognizer *)gestureRecognizer
{
    UIButton *btn = (UIButton *) gestureRecognizer.view;
    NSLog(@"handlePinButtonTap: ann.title=%d", btn.tag);
    [self navigateToListView:btn.tag];
}

-(void)navigateToListView:(int)garageIndex
{
    GarageDetailObject* garDObject = (GarageDetailObject*)[itemsArry objectAtIndex:garageIndex];
    //if (![appDel.recentVisitedArr containsObject:[filterArr objectAtIndex:garageIndex]]) {
    //    [appDel.recentVisitedArr addObject:[filterArr objectAtIndex:garageIndex]];
    //}
    
    
    //GarageView* gView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GarageView"];
    //[self.navigationController pushViewController:gView animated:YES];
    //[gView garageDetailId:garObject._garageId];
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    //NSLog(@"MAp view %d",view.tag);
    
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
    [Utility showLoadingIndicator:@"" withView:self.view];
    
    annView = (MKAnnotationView *)[mapView1
                                   dequeueReusableAnnotationViewWithIdentifier: @"pin"];
    annView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                           reuseIdentifier:@"pin"];
    
    [annView setFrame:CGRectMake(annView.frame.origin.x, annView.frame.origin.y,50,50)];
    
    pinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    pinButton.frame = CGRectMake(0,0,50,50);
    GarageDetailObject* garObj1;
    
   
    
    [pinButton setBackgroundImage:[UIImage imageNamed:@"map-pin-atm.png"] forState:UIControlStateNormal];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handlePinButtonTap:)];
    tap.numberOfTapsRequired = 1;
    [pinButton addGestureRecognizer:tap];
    [annView addSubview:pinButton];
    
    UILabel* ptsLbl = [[UILabel alloc] initWithFrame:CGRectMake(annView.frame.origin.x, annView.frame.origin.y+8, 50, 20)];
    ptsLbl.textAlignment = NSTextAlignmentCenter;
    ptsLbl.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0];
    [ptsLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
    
    if(annotation == [mapView1.annotations objectAtIndex:[mapView1.annotations indexOfObject:annotation]]){
        pinButton.tag = [mapView1.annotations indexOfObject:annotation];
        
        garObj1 = (GarageDetailObject*)[itemsArry objectAtIndex:[mapView1.annotations indexOfObject:annotation]];
        
       // NSLog(@"Str tag %d %@ \n Ann %lu \n CAnn %@",[mapView1.annotations indexOfObject:annotation],garObj1._currntRate,(unsigned long)[mapView1.annotations objectAtIndex:[mapView1.annotations indexOfObject:annotation]],annotation);
        
        
        ptsLbl.text = [NSString stringWithFormat:@"$%@",garObj1._currntRate];
        //[annView addSubview:ptsLbl];
        annView.annotation = annotation;
        
    }
    UIButton *pb = (UIButton *)[annView viewWithTag:10];
    [pb setTitle:annotation.title forState:UIControlStateNormal];
    [Utility stopLoading:self.view];
    
    return annView;
}

-(void)mapPopUp:(id)sender withAnno:(MKAnnotationView*)av
{
    UIButton* _btn = (UIButton*)sender;
    //buttonIndex = _btn.tag;
    
    pinButton = sender;
    annView = av;
    
	if(annView != nil){
		myview.hidden = YES;
		myview = nil;
	}
    NSLog(@"Its working");
    
    mapPopView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,300,300)];
    
    userLocLbl = [[UILabel alloc]initWithFrame:CGRectMake(40,10,220,40)];
    [userLocLbl setFont:[UIFont boldSystemFontOfSize:16.0]];
    
    [userLocLbl setText:@"ArunSir"];
    userLocLbl.numberOfLines = 2;
    
    myview = [[UIView alloc]initWithFrame:CGRectMake(-122,-270,300,300)];
    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [closeBtn addTarget:self
                 action:@selector(closeBtn:)
       forControlEvents:UIControlEventTouchDown];
    
    [myview addSubview:userLocLbl];
    [annView addSubview:myview];
    [myview addSubview:mapPopView];
    [myview sendSubviewToBack:mapPopView];
    [annView addSubview:myview];
}

-(void)closeBtn:(id)sender{
    
    if(annView != nil)
    {
		myview.hidden = YES;
		myview = nil;
	}
    
}

//#pragma mark - CLLocationManagerDelegate
//
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    NSLog(@"didFailWithError: %@", error);
//    //UIAlertView *errorAlert = [[UIAlertView alloc]
//    //                           initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    //[errorAlert show];
//    
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocation *currentLocation = newLocation;
//    
//    if (currentLocation != nil) {
//        
//        NSLog(@"Longitute:%@ and Latitute:%@",[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude],[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
//        
//        //if(!filterArr.count)
//        //   [self initGarageMapView];
//        //longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//        //latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//    }
//    
//    
//    // Stop Location Manager
//    [locationManager stopUpdatingLocation];
//    
//    // Reverse Geocoding
//    NSLog(@"Resolving the Address");
//    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
//        if (error == nil && [placemarks count] > 0) {
//            placemark = [placemarks lastObject];
//            NSLog(@"Address is:%@ %@\n%@ %@\n%@\n%@",
//                  placemark.subThoroughfare, placemark.thoroughfare,
//                  placemark.postalCode, placemark.locality,
//                  placemark.administrativeArea,
//                  placemark.country);
//        } else {
//            NSLog(@"%@", error.debugDescription);
//        }
//    } ];
//}



#pragma mark- ServerConnect Delegate
- (void) dataLoadRequestFailedWithError:(NSError*)error andMsg:(NSString*)errorMsg;
{
    NSLog(@"Error in Garage View Controller");
}
- (void) dictLoadedFromServer:(NSDictionary*)dict;
{
    [Utility stopLoading:self.view];
    for (int i=0; i<[[dict valueForKey:@"Items"] count]; i++) {
        GarageDetailObject* garDetObj = [GarageDetailObject garageDetailFromDict:[[dict valueForKey:@"Items"] objectAtIndex:i]];
        [itemsArry addObject:garDetObj];
    }
    [self showDetailsOfGarage];
    [self mapViewDetails];
}

- (void) stringLoadedFromServer:(NSString*)str;
{
    
}
- (void) arrayLoadedFromServer:(NSArray*)Array;
{
    NSLog(@"Received array is %@",Array);
    
}

#pragma mark - UIButton Actions
- (IBAction)favoriteBtnClicked:(id)sender {
    
    UIButton* btn = (UIButton*)sender;
    
    AppDelegate* appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if(btn.selected){
        
        if ([appDel.favGarageArr containsObject:garageObj]) {
            [appDel.favGarageArr removeObject:garageObj];
        }

        [btn setImage:[UIImage imageNamed:@"btn-favorite2-0"] forState:UIControlStateNormal];
        btn.selected = NO;
        garageObj.favKey = NO;
    }
    else{
        
        if (![appDel.favGarageArr containsObject:garageObj]) {
            [appDel.favGarageArr addObject:garageObj];
        }

        [btn setImage:[UIImage imageNamed:@"btn-favorite2-1"] forState:UIControlStateNormal];
        btn.selected = YES;
        garageObj.favKey = YES;
    }
    
}

- (IBAction)navigationBtnClicked:(id)sender {
    NavigationDecisionScreen* navDScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationDecisionScreen"];
    [navDScreen destinationAddress:currentAddress withLatitute:latitu andLongitute:longit];
    [self.navigationController pushViewController:navDScreen animated:YES];
}

#pragma mark  -Dynamic Label WidthHeight
-(UILabel*)setNewFrameForLabel:(UILabel*)lbl
{
    
    CGSize maximumLabelSize = CGSizeMake(300,9999);
    CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font
                                    constrainedToSize:maximumLabelSize
                                        lineBreakMode:lbl.lineBreakMode];
    CGRect newFrame = lbl.frame;
    newFrame.size.height = expectedLabelSize.height;
    newFrame.size.width = expectedLabelSize.width;
    [lbl setFrame:newFrame];
    return lbl;
}

-(IBAction)reserveParkTaped:(id)sender{
    NSArray* array = [[NSBundle mainBundle] loadNibNamed:@"RedeemCouponView" owner:self options:nil];
    couponView = (RedeemCouponView *)[array objectAtIndex:0];
    [couponView.btn_Redeem addTarget:self action:@selector(clickToRedeem:) forControlEvents:UIControlEventTouchUpInside];
    couponView.layer.cornerRadius = 5.0f;
    couponView.layer.masksToBounds = YES;
    [self.view addSubview:couponView];

}

- (void)clickToRedeem:(UIButton *)sender {
    UIButton *btn = (UIButton *)sender;
    
    if (btn.tag == 0) {
        btn.tag = 1;
        [btn setTitle:@"Redeem" forState:UIControlStateNormal];
        couponView.couponViewObj.hidden = NO;
        couponView.pickerViewObj.hidden = YES;
    }
    else if (btn.tag == 1) {
        btn.tag = 0;
        [self redeemCouponTaped:nil];
        [couponView removeFromSuperview];
    }
}


- (IBAction)redeemCouponTaped:(id)sender{
    NSArray* array = [[NSBundle mainBundle] loadNibNamed:@"QRCouponView" owner:self options:nil];
    qrViewObj = (QRCouponView *)[array objectAtIndex:0];
    [qrViewObj.btn_QR addTarget:self action:@selector(clickToHide:) forControlEvents:UIControlEventTouchUpInside];
    qrViewObj.layer.cornerRadius = 5.0f;
    qrViewObj.layer.masksToBounds = YES;
    [self.view addSubview:qrViewObj];
    
}

- (void)clickToHide:(UIButton *)sender{
    [qrViewObj removeFromSuperview];
}


@end
