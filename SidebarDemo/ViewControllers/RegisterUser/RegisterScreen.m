//
//  RegisterScreen.m
//  Park
//
//  Created by Devendra Singh on 6/6/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "RegisterScreen.h"
#import "Utility.h"

@interface RegisterScreen ()

@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (weak, nonatomic) IBOutlet UITextField *_nameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *_userNameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *_passwordTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *_confirmPassTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *_emailTxtFld;
@end

@implementation RegisterScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self._scrollView setContentSize:CGSizeMake(320, 650)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(headerBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    
    
    
    //    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    //    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    //    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    //
    //    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [navRightView addSubview:searchButton];
    //
    //    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //    //[navRightView addSubview:titleSepratorImg];
    //
    //    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    //    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"SusaBold" size:23.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Sign up";
    self.navigationItem.titleView = label;
    
}

@end
