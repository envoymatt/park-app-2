//
//  HomeViewController.m
//  Park.com
//
//  Created by Mohd. Rahib on 03/09/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()
- (IBAction)registerClicked:(id)sender;
- (IBAction)loginUserClicked:(id)sender;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtonActions
- (IBAction)loginUserClicked:(id)sender {
    LoginUserScreen* logScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginUserScreen"];
    [self.navigationController pushViewController:logScreen animated:YES];
}

- (IBAction)registerClicked:(id)sender {
    RegisterScreen* rScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"RegisterScreen"];
    [self.navigationController pushViewController:rScreen animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
