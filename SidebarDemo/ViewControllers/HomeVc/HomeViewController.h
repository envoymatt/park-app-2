//
//  HomeViewController.h
//  Park.com
//
//  Created by Mohd. Rahib on 03/09/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterScreen.h"
#import "LoginUserScreen.h"


@interface HomeViewController : UIViewController

@end
