//
//  FilterScreen.m
//  Park.com
//
//  Created by Devendra Singh on 6/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "FilterScreen.h"
#import "Utility.h"

@interface FilterScreen ()
@property (weak, nonatomic) IBOutlet UIScrollView *_scrlView;
@property (weak, nonatomic) IBOutlet UIImageView *_groceryImg;
@property (weak, nonatomic) IBOutlet UILabel *_groceryLbl;
@property (weak, nonatomic) IBOutlet UIButton *_groceryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_bikeRentalImg;
@property (weak, nonatomic) IBOutlet UILabel *_bikeRentalLbl;
@property (weak, nonatomic) IBOutlet UIButton *_bikeRentalBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_barImg;
@property (weak, nonatomic) IBOutlet UILabel *_barLbl;
@property (weak, nonatomic) IBOutlet UIButton *_barBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_foodImg;
@property (weak, nonatomic) IBOutlet UILabel *_foodLbl;
@property (weak, nonatomic) IBOutlet UIButton *_foodBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_gasImg;
@property (weak, nonatomic) IBOutlet UILabel *_gasLbl;
@property (weak, nonatomic) IBOutlet UIButton *_gasBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_AtmImg;
@property (weak, nonatomic) IBOutlet UILabel *_AtmLbl;
@property (weak, nonatomic) IBOutlet UIButton *_AtmBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_theaterImg;
@property (weak, nonatomic) IBOutlet UILabel *_theaterLbl;
@property (weak, nonatomic) IBOutlet UIButton *_theaterBtn;
@property (weak, nonatomic) IBOutlet UIImageView *_hotelImg;
@property (weak, nonatomic) IBOutlet UILabel *_hotelLbl;
@property (weak, nonatomic) IBOutlet UIButton *_hotelBtn;

- (IBAction)groceryBtnClicked:(id)sender;
- (IBAction)bikeRentalBtnClicked:(id)sender;
- (IBAction)barBtnClicked:(id)sender;
- (IBAction)foodBtnClicked:(id)sender;
- (IBAction)gasBtnClicked:(id)sender;
- (IBAction)atmBtnClicked:(id)sender;
- (IBAction)theaterBtnClicked:(id)sender;
- (IBAction)hotelBtnClicked:(id)sender;


@end

@implementation FilterScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavigationBar];
    
//    int x= 18;
//    for (int i=0; i<8; i++) {
//    UIButton *custButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    custButton.frame = CGRectMake(270, x, 25, 15);
//    [custButton setTitle:@"dev" forState:UIControlStateNormal];
//    [custButton setBackgroundColor:[UIColor redColor]];
//    custButton.tag = i;
//    [custButton addTarget:self
//                       action:@selector(selectedBtn:)
//             forControlEvents:UIControlEventTouchUpInside];
//        
//    x=x+50;
//    [self._scrlView addSubview:custButton];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(IBAction)selectedBtn:(id)sender
//{
//    UIButton* btn = (UIButton*)sender;
//    NSLog(@"Selected Button: %ld",(long)btn.tag);
//    [btn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
//}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 45, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 45, 22)];
    [backButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
    [backButton.titleLabel setTextColor:[UIColor whiteColor]];
    [backButton addTarget:self action:@selector(headerCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    UIButton* applyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [applyButton setFrame:CGRectMake(0, 0, 60, 34)];
    [applyButton setTitle:@"Apply" forState:UIControlStateNormal];
    [applyButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    [applyButton addTarget:self action:@selector(applyBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:applyButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerCancelButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)applyBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Filters";
    self.navigationItem.titleView = label;
    
}

#pragma mark - UIButton Actions
- (IBAction)groceryBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._groceryBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._groceryLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._groceryImg setImage:[UIImage imageNamed:@"cartSelected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._groceryBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._groceryLbl.textColor = [UIColor darkGrayColor];
        [self._groceryImg setImage:[UIImage imageNamed:@"groceryIcon.png"]];

    }
}

- (IBAction)bikeRentalBtnClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._bikeRentalBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._bikeRentalLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._bikeRentalImg setImage:[UIImage imageNamed:@"bicycleselected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._bikeRentalBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._bikeRentalLbl.textColor = [UIColor darkGrayColor];
        [self._bikeRentalImg setImage:[UIImage imageNamed:@"bicycle.png"]];

    }
}

- (IBAction)barBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._barBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._barLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._barImg setImage:[UIImage imageNamed:@"barIcon.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._barBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._barLbl.textColor = [UIColor darkGrayColor];
        [self._barImg setImage:[UIImage imageNamed:@"barunslected.png"]];

    }
}

- (IBAction)foodBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._foodBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._foodLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._foodImg setImage:[UIImage imageNamed:@"foodselected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._foodBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._foodLbl.textColor = [UIColor darkGrayColor];
        [self._foodImg setImage:[UIImage imageNamed:@"foodIcon.png"]];

    }
}

- (IBAction)gasBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._gasBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._gasLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._gasImg setImage:[UIImage imageNamed:@"gasSelected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._gasBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._gasLbl.textColor = [UIColor darkGrayColor];
        [self._gasImg setImage:[UIImage imageNamed:@"gasIcon.png"]];

    }
}

- (IBAction)atmBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._AtmBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._AtmLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._AtmImg setImage:[UIImage imageNamed:@"atmSelected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._AtmBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._AtmLbl.textColor = [UIColor darkGrayColor];
        [self._AtmImg setImage:[UIImage imageNamed:@"atmIcon.png"]];

    }
}

- (IBAction)theaterBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._theaterBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._theaterLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._theaterImg setImage:[UIImage imageNamed:@"theaterIcon.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._theaterBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._theaterLbl.textColor = [UIColor darkGrayColor];
        [self._theaterImg setImage:[UIImage imageNamed:@"theaterunselected.png"]];

    }
}

- (IBAction)hotelBtnClicked:(id)sender {
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0) {
        btn.tag = 1;
        [self._hotelBtn setBackgroundImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
        self._hotelLbl.textColor = [UIColor colorWithRed:97/255.0 green:166/255.0 blue:70/255.0 alpha:1.0];
        [self._hotelImg setImage:[UIImage imageNamed:@"hotelselected.png"]];
    }
    else if (btn){
        btn.tag = 0;
        [self._hotelBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self._hotelLbl.textColor = [UIColor darkGrayColor];
        [self._hotelImg setImage:[UIImage imageNamed:@"hotelIcon.png"]];

    }
}
@end
