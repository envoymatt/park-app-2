//
//  NavigationPath.h
//  Park
//
//  Created by Devendra Singh on 6/6/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NavigationPath : UIViewController<MKMapViewDelegate>


-(void)showRouteLat:(NSString*)CLat Long:(NSString*)CLong withDestinationAdd:(NSString*)add;


@end
