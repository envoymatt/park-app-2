//
//  NavigationPath.m
//  Park
//
//  Created by Devendra Singh on 6/6/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "NavigationPath.h"
#import "Utility.h"



@interface NavigationPath ()
{
    IBOutlet MKMapView *routeMap;
    
    NSString* destiAddres;
}
@end

@implementation NavigationPath

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [routeMap setDelegate:self];
    routeMap.showsUserLocation = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MapRoute Implementation
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor blueColor];
    polylineView.lineWidth = 6.0;
    
    return polylineView;
}
- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    
    return CLLocationCoordinate2DMake(latitude, longitude);
}

-(void)showRouteLat:(NSString*)CLat Long:(NSString*)CLong withDestinationAdd:(NSString*)add;
{
    destiAddres = add;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
    MKCoordinateRegion region = MKCoordinateRegionMake(userLocation.location.coordinate, span);
    
    [mapView setRegion:region];
    
    [mapView setCenterCoordinate:userLocation.coordinate animated:YES];
    
    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%@&sensor=true", mapView.userLocation.location.coordinate.latitude,  mapView.userLocation.location.coordinate.longitude, destiAddres];
    
    NSURL *url = [NSURL URLWithString:[baseUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSError *error = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSArray *routes = [result objectForKey:@"routes"];
        
        NSDictionary *firstRoute = [routes objectAtIndex:0];
        
        NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
        
        NSDictionary *end_location = [leg objectForKey:@"end_location"];
        
        double latitude = [[end_location objectForKey:@"lat"] doubleValue];
        double longitude = [[end_location objectForKey:@"lng"] doubleValue];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coordinate;
        point.title =  [leg objectForKey:@"end_address"];
        //point.subtitle = @"I'm here!!!";
        
        [routeMap addAnnotation:point];
        
        NSArray *steps = [leg objectForKey:@"steps"];
        
        int stepIndex = 0;
        
        CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
        
        stepCoordinates[stepIndex] = userLocation.coordinate;
        
        for (NSDictionary *step in steps) {
            
            NSDictionary *start_location = [step objectForKey:@"start_location"];
            stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
            
            if ([steps count] == stepIndex){
                NSDictionary *end_location = [step objectForKey:@"end_location"];
                stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
            }
        }
        
        MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:stepCoordinates count:1 + stepIndex];
        [mapView addOverlay:polyLine];
        
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake((userLocation.location.coordinate.latitude + coordinate.latitude)/2, (userLocation.location.coordinate.longitude + coordinate.longitude)/2);
        
    }];
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(headerBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    
    
    
    //    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    //    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    //    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    //
    //    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [navRightView addSubview:searchButton];
    //
    //    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //    //[navRightView addSubview:titleSepratorImg];
    //
    //    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    //    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Route";
    self.navigationItem.titleView = label;
    
}


@end
