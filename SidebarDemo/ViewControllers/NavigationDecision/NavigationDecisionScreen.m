//
//  NavigationDecisionScreen.m
//  Park
//
//  Created by Devendra Singh on 6/6/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "NavigationDecisionScreen.h"
#import "Utility.h"
#import "NavigationPath.h"

@interface NavigationDecisionScreen ()
{
//    CLLocationManager* locationManager;
//    CLGeocoder *geocoder;
//    CLPlacemark *placemark;
    
    NSString* destAdd;
    
    double currentLat;
    double currentLong;
    NSString* destiLat;
    NSString* destiLong;
    
    
    
}
- (IBAction)walkingClicked:(id)sender;
- (IBAction)drivingClicked:(id)sender;

@end

@implementation NavigationDecisionScreen
@synthesize locationManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)destinationAddress:(NSString*)add withLatitute:(NSString*)lat andLongitute:(NSString*)longi
{
    destAdd = add;
    destiLat = lat;
    destiLong = longi;
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(headerBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    
    
    
    //    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    //    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    //    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    //
    //    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [navRightView addSubview:searchButton];
    //
    //    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //    //[navRightView addSubview:titleSepratorImg];
    //
    //    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    //    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Navigation Decision";
    self.navigationItem.titleView = label;
    
}

/*
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    //UIAlertView *errorAlert = [[UIAlertView alloc]
     //                          initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[errorAlert show];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        NSLog(@"Longitute:%@ and Latitute:%@",[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude],[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
        
        //if(!filterArr.count)
        //   [self initGarageMapView];
        //longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        //latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    
    currentLatiStr = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    currentLongiStr = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    // Stop Location Manager
    [locationManager stopUpdatingLocation];
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            NSLog(@"Address is:%@ %@\n%@ %@\n%@\n%@",
                  placemark.subThoroughfare, placemark.thoroughfare,
                  placemark.postalCode, placemark.locality,
                  placemark.administrativeArea,
                  placemark.country);
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
}
*/


#pragma mark - UIButton Actions
- (IBAction)walkingClicked:(id)sender {
    
    [self makeDirectionOnDefaultMap];
    return;

//    NavigationPath* navPath = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationPath"];
//    [self.navigationController pushViewController:navPath animated:YES];
//    
//    [navPath showRouteLat:@"" Long:@"" withDestinationAdd:destAdd];
    
    
}

- (IBAction)drivingClicked:(id)sender {
    //[self makeRouteWithLatitute:destAdd andLongitute:destLongiStr];
    [self makeDirectionOnDefaultMap];
    return;
//    NavigationPath* navPath = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationPath"];
//    [self.navigationController pushViewController:navPath animated:YES];
//    [navPath showRouteLat:@"" Long:@"" withDestinationAdd:destAdd];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    //self.locationString=[NSString stringWithFormat:@"%f,%f",self.manager.location.coordinate.latitude,self.manager.location.coordinate.longitude];
    currentLat = self.locationManager.location.coordinate.latitude;
    currentLong = self.locationManager.location.coordinate.longitude;
    [manager stopUpdatingLocation];

    
}

- (void)makeDirectionOnDefaultMap
{
    
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:currentLat longitude:currentLong];
    
    //NSString *name = [_locationItem.name length] ?_locationItem.name :_locationItem.MERCH_NAM;
    if (([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending))
    {
        NSString *urlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?ll=%f,%f&q=%@@%f,%f",
                            userLocation.coordinate.latitude, userLocation.coordinate.longitude, destAdd, [destiLat doubleValue], [destiLong doubleValue]];
        NSURL *mapsUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:mapsUrl];
    }
    else
    {
        CLLocationCoordinate2D itemLocation = {[destiLat doubleValue], [destiLong doubleValue]};
        MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:userLocation.coordinate addressDictionary:nil];
        MKPlacemark* currentPlacemark = [[MKPlacemark alloc] initWithCoordinate:itemLocation addressDictionary:nil];
        
        MKMapItem* item = [[MKMapItem alloc] initWithPlacemark:placemark];
        [item setName:NSLocalizedString(@"Current Location", nil)];
        
        MKMapItem* currentLocationMapItem = [[MKMapItem alloc] initWithPlacemark:currentPlacemark];
        [currentLocationMapItem setName:destAdd];
        NSDictionary* mapLaunchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
        
        [MKMapItem openMapsWithItems:@[item, currentLocationMapItem] launchOptions:mapLaunchOptions];
    }
}


@end
