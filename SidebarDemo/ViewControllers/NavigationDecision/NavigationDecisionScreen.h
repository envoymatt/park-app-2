//
//  NavigationDecisionScreen.h
//  Park
//
//  Created by Devendra Singh on 6/6/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NavigationDecisionScreen : UIViewController<CLLocationManagerDelegate>
@property (nonatomic,retain)CLLocationManager *locationManager;


-(void)destinationAddress:(NSString*)add withLatitute:(NSString*)lat andLongitute:(NSString*)longi ;
@end
