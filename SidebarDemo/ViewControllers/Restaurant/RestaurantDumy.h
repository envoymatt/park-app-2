//
//  RestaurantDumy.h
//  Park.com
//
//  Created by Devendra Singh on 20/08/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantDumy : UIViewController<UIPageViewControllerDataSource>

@end
