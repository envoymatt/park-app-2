//
//  RestaurantNameScreen.m
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "RestaurantNameScreen.h"
#import "Utility.h"
#import "NavigationDecisionScreen.h"


@interface RestaurantNameScreen ()
{
    NSArray* couponImageArray;
    NSArray* couponTypeArray;
    NSArray* couponPriceArray;
}
@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (strong, nonatomic) UIPageViewController *pageController;
@property (weak, nonatomic) IBOutlet UIImageView *_couponsImgView;
@property (weak, nonatomic) IBOutlet UILabel *_couponTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *_couponPriceLbl;


@end

@implementation RestaurantNameScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self._scrollView setContentSize:CGSizeMake(320, 1000)];
 
    couponImageArray = [[NSArray alloc] initWithObjects:@"marriott.png",@"wellness.png",@"gotham.png", nil];
    couponTypeArray = [[NSArray alloc] initWithObjects:@"Hotel Name",@"Spa Name",@"Restaurant Name", nil];
    couponPriceArray = [[NSArray alloc] initWithObjects:@"$20 off per night",@"10% off all services",@"$5 wings!", nil];
    self._couponsImgView.image = [UIImage imageNamed:[couponImageArray objectAtIndex:self.index]];
    self._couponTypeLbl.text = [couponTypeArray objectAtIndex:self.index];
    self._couponPriceLbl.text =[couponPriceArray objectAtIndex:self.index];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
