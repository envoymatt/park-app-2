//
//  RestaurantDumy.m
//  Park.com
//
//  Created by Devendra Singh on 20/08/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "RestaurantDumy.h"
#import "RestaurantNameScreen.h"
#import "Utility.h"

@interface RestaurantDumy ()
@property (strong, nonatomic) UIPageViewController *pageController;

@end

@implementation RestaurantDumy

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:78/255.0 green:151/255.0 blue:50/255.0 alpha:1.0];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    RestaurantNameScreen *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Pagination
- (RestaurantNameScreen *)viewControllerAtIndex:(NSUInteger)index {
    
    RestaurantNameScreen* childViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"RestaurantNameScreen"];
    childViewController.index = index;
    
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(RestaurantNameScreen *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(RestaurantNameScreen *)viewController index];
    
    index++;
    
    if (index == 3) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(headerBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navLeftView addSubview:titleSepratorImg];
    
    
    
    
    //    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    //    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    //    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    //
    //    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [navRightView addSubview:searchButton];
    //
    //    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //    //[navRightView addSubview:titleSepratorImg];
    //
    //    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    //    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}
-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"Restaurant Name";
    self.navigationItem.titleView = label;
    
}

@end
