//
//  RestaurantNameScreen.h
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantNameScreen : UIViewController<UIPageViewControllerDataSource>
@property (assign, nonatomic) NSInteger index;

@end
