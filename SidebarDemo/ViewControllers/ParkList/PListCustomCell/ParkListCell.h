//
//  ParkListCell.h
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ParkListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *_detailView;
@property (weak, nonatomic) IBOutlet UILabel *_noRecordsLbl;
@property (weak, nonatomic) IBOutlet UILabel *_garageNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *_addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *_cityStateZipLbl;
@property (weak, nonatomic) IBOutlet UILabel *_phoneNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *_typeLbl;
@property (weak, nonatomic) IBOutlet UIButton *_btnCost;
@property (weak, nonatomic) IBOutlet UIButton *_favBtn;
@property (weak, nonatomic) IBOutlet UIButton *_navigationBtn;


@end
