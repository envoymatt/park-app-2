//
//  ParkNearByScreen.m
//  Park
//
//  Created by Devendra Singh on 6/5/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "ParkNearByScreen.h"
#import "Utility.h"
#import "SWRevealViewController.h"
#import "FilterScreen.h"
#import "NearByCustomCell.h"
#import "ServerConnect.h"
#import "ParkListCell.h"
#import "NearByObject.h"
//#import "RestaurantNameScreen.h"
#import "NavigationDecisionScreen.h"
#import "RestaurantDumy.h"

#define MAP_UPDATE_DISTANCE 200.00 // in meters
#define MAP_VIEW_REGION_DISTANCE 50.0 // in meters

#define NY_LATITUDE 40.714353
#define NY_LONGTITUDE -74.005973

@interface ParkNearByScreen ()
{
    BOOL isSearching;
    NSArray* filteredArray;
    
    NSString* cellIdentifier;
    NSDictionary* responseDict;
    CLLocation* _location;
    MKAnnotationView *annView;
    int buttonIndex;
    UIImageView* mapPopView;
    UILabel* userLocLbl;
    UIView* myview;
    UIButton* closeBtn;
    
    int pageCnt;
    NSMutableArray* itemsArryNearBy;
    CLLocationCoordinate2D userLocation;

}
@property (weak, nonatomic) IBOutlet UITableView *_tablViewList;
@property (weak, nonatomic) IBOutlet UIView *_listView;
@property (weak, nonatomic) IBOutlet UIView *_mapView;
@property (weak, nonatomic) IBOutlet MKMapView *_mkMapView;
- (IBAction)filterClicked:(id)sender;

@end

@implementation ParkNearByScreen
{
    //CLLocationManager* locationManager;
    //CLGeocoder *geocoder;
    //CLPlacemark *placemark;
    
}
@synthesize _tablViewList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)gotoNewYorkLocation
{
    MKCoordinateRegion newRegion;
    
    newRegion.center.latitude = NY_LATITUDE;
    newRegion.center.longitude = NY_LONGTITUDE;
    
    newRegion.span.latitudeDelta = 0.2f;
    newRegion.span.longitudeDelta = 0.2f;
    
    [self._mkMapView setRegion:newRegion animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated
{
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self._mkMapView.showsUserLocation = YES;
    [self initParkNearView];
    
    pageCnt = 1;
    itemsArryNearBy = [[NSMutableArray alloc]init];
    
    [self setupNavigationBar];
    cellIdentifier = @"Cell";
    [self._tablViewList registerNib:[UINib nibWithNibName:@"ParkListCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

-(void)initParkNearView{
    [Utility showLoadingIndicator:@"" withView:self.view];
    //[self performSelector:@selector(apiCallNearBy) withObject:nil afterDelay:0.5];
    [self nearMeNowApiCallInBackground];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - MapView Delegate
-(void)mapViewDetails
{
    NSMutableArray* arr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<[itemsArryNearBy count];i++)
    {
        NearByObject* nearBObj = (NearByObject*)[itemsArryNearBy objectAtIndex:i];
        _location = [[CLLocation alloc]initWithLatitude:[nearBObj._latitude floatValue] longitude:[nearBObj._longitute floatValue]];
        
        [arr addObject:_location];
    }
    //[arr addObject:[[CLLocation alloc]initWithLatitude:[[[mapArr objectAtIndex:0]valueForKey:@"Latitude"]floatValue] longitude:[[[mapArr objectAtIndex:0]valueForKey:@"Longitude"]floatValue]]];
    //[self drawRoute:arr];
    [self annotationPinDetails];
    
}

-(void)annotationPinDetails
{
    for(int i=0;i<[itemsArryNearBy count];i++)
    {
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake(0, 0);
        
        NearByObject* nearObj = (NearByObject*)[itemsArryNearBy objectAtIndex:i];
        if(nearObj._longitute != nil && nearObj._longitute != [NSNull null])
        {
            annotationCoord.latitude =  [nearObj._latitude floatValue];
            annotationPoint.coordinate = annotationCoord;
            
        }
        if(nearObj._longitute != nil && nearObj._longitute != [NSNull null])
        {
            annotationCoord.longitude = [nearObj._longitute floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        annotationPoint.title = [NSString stringWithFormat:@"%d",i];
        [self._mkMapView addAnnotation:annotationPoint];
    }
    [self zoomIn:self._mkMapView];
}

-(void)zoomIn:(MKMapView*)mapView
{
    MKCoordinateRegion region = mapView.region;
    region.span.latitudeDelta = region.span.latitudeDelta/1500;
    region.span.longitudeDelta = region.span.longitudeDelta/1500;
    region.center.latitude  = 40.80458069;
    region.center.longitude = -73.9359436;
    [mapView setRegion:region animated:YES];
}


- (void)handlePinButtonTap:(UITapGestureRecognizer *)gestureRecognizer
{
    UIButton *btn = (UIButton *) gestureRecognizer.view;
    MKAnnotationView *av = (MKAnnotationView *)[btn superview];
    NSLog(@"handlePinButtonTap: ann.title=%ld", (long)av.tag);
    //[self mapPopUp:btn withAnno:av];
    if ([self hp_isEqualToImage:btn.imageView.image]) {
        RestaurantDumy* parkFltrScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"RestaurantDumy"];
        [self.navigationController pushViewController:parkFltrScreen animated:YES];
    }
    else{
        UIAlertView* alertVw = [[UIAlertView alloc] initWithTitle:@"Message" message:@"This app only deals with garages specifically. The future build will show other points of interest such as this one." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertVw show];
    }
    
    
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
    //[Utility showLoadingIndicator:@"" withView:self.view];
    //NSLog(@"Ann Pin %lu",(unsigned long)[mapView1.annotations indexOfObject:annotation]);
    
    static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
    if (annotation == self._mkMapView.userLocation){
        MKAnnotationView* annotationView;
        userLocation = self._mkMapView.userLocation.coordinate;
		// We can return nil to let the MapView handle the default annotation view (blue dot):
		// return nil;
        
		// Or instead, we can create our own blue dot and even configure it:
        
		annotationView = [mapView1 dequeueReusableAnnotationViewWithIdentifier:@"blueDot"];
		if (annotationView != nil){
			annotationView.annotation = annotation;
		}
		else{
			annotationView = [[NSClassFromString(@"MKUserLocationView") alloc] initWithAnnotation:annotation reuseIdentifier:@"blueDot"];
		}
	}
    else{
        
        NSLog(@"MAP %lu",(unsigned long)mapView1.annotations.count);
        
        MKAnnotationView *annotationView = [mapView1 dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
        
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:kPinAnnotationIdentifier];
        
        //update view's annotation to current in case it's being re-used
        //annotationView.annotation = annotation;
        
        //annotationView.enabled = YES;
        
        
        
        [annotationView setFrame:CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y,50,50)];
        
        UIButton *pinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pinButton.frame = CGRectMake(0,0,50,50);
        NearByObject* nearObj1;
        
                switch ([mapView1.annotations indexOfObject:annotation]) {
                    case 0:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-H.png"] forState:UIControlStateNormal];
                        break;
        
                    case 1:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
        
                    case 2:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-pub.png"] forState:UIControlStateNormal];
                        break;
        
                    case 3:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-movie.png"] forState:UIControlStateNormal];
                        break;
        
                    case 4:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-shopping.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 5:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 6:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
                    case 7:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-movie.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 8:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;

                    case 9:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-atm.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 10:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
        
                    case 11:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-cycle.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 12:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 13:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-cycle.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 14:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-atm.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 15:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
                        break;
                        
                    case 16:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-movie.png"] forState:UIControlStateNormal];
                        break;

                    default:
                        [pinButton setImage:[UIImage imageNamed:@"map-pin-fuel.png"] forState:UIControlStateNormal];
                        break;
                }
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handlePinButtonTap:)];
        tap.numberOfTapsRequired = 1;
        [pinButton addGestureRecognizer:tap];
        [annotationView addSubview:pinButton];
        
        UILabel* ptsLbl = [[UILabel alloc] initWithFrame:CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y+8, 50, 20)];
        ptsLbl.textAlignment = NSTextAlignmentCenter;
        ptsLbl.backgroundColor = [UIColor clearColor];
        ptsLbl.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0];
        [ptsLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10.0]];
        
        if(annotation == [mapView1.annotations objectAtIndex:[mapView1.annotations indexOfObject:annotation]]){
            
            if([mapView1.annotations indexOfObject:annotation] < [itemsArryNearBy count]){
                pinButton.tag = [mapView1.annotations indexOfObject:annotation];
                nearObj1 = (NearByObject*)[itemsArryNearBy objectAtIndex:[mapView1.annotations indexOfObject:annotation]];
            }
            else{
                nearObj1 = (NearByObject*)[itemsArryNearBy objectAtIndex:[mapView1.annotations indexOfObject:annotation]- 1];
                pinButton.tag = [mapView1.annotations indexOfObject:annotation] - 1;
            }
            
            //ptsLbl.text = [NSString stringWithFormat:@"$%@",nearObj1._currntRate];
            
            [annotationView addSubview:ptsLbl];
            annotationView.annotation = annotation;
        }
        UIButton *pb = (UIButton *)[annotationView viewWithTag:10];
        [pb setTitle:annotation.title forState:UIControlStateNormal];
        [Utility stopLoading:self.view];
        
        return annotationView;
    }
    return nil;
}



//- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
//{
//    NSLog(@"MAp View tag %@",annotation);
//    static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
//    if (annotation == self._mkMapView.userLocation){
//        MKAnnotationView* annotationView;
//        userLocation = self._mkMapView.userLocation.coordinate;
//		// We can return nil to let the MapView handle the default annotation view (blue dot):
//		// return nil;
//        
//		// Or instead, we can create our own blue dot and even configure it:
//        
//		annotationView = [mapView1 dequeueReusableAnnotationViewWithIdentifier:@"blueDot"];
//		if (annotationView != nil){
//			annotationView.annotation = annotation;
//		}
//		else{
//			annotationView = [[NSClassFromString(@"MKUserLocationView") alloc] initWithAnnotation:annotation reuseIdentifier:@"blueDot"];
//		}
//	}
//    else{
//    annView = (MKAnnotationView *)[mapView1
//                                   dequeueReusableAnnotationViewWithIdentifier: @"pin"];
//    if (annView == nil)
//    {
//        annView = [[MKAnnotationView alloc] initWithAnnotation:annotation
//                                               reuseIdentifier:@"pin"];
//        
//        [annView setFrame:CGRectMake(annView.frame.origin.x, annView.frame.origin.y,50,50)];
//        
//        pinButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        pinButton.frame = CGRectMake(0,0,50,50);
//        
//        
//        switch ([mapView1.annotations indexOfObject:annotation]) {
//            case 0:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-H.png"] forState:UIControlStateNormal];
//                break;
//                
//            case 1:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
//                break;
//                
//            case 2:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-restaurant.png"] forState:UIControlStateNormal];
//                break;
//                
//            case 3:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-movie.png"] forState:UIControlStateNormal];
//                break;
//                
//            case 4:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-shopping.png"] forState:UIControlStateNormal];
//                break;
//                
//            default:
//                [pinButton setImage:[UIImage imageNamed:@"map-pin-fuel.png"] forState:UIControlStateNormal];
//                break;
//        }
//        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                       initWithTarget:self action:@selector(handlePinButtonTap:)];
//        tap.numberOfTapsRequired = 1;
//        [pinButton addGestureRecognizer:tap];
//        [annView addSubview:pinButton];
//        
//        
//        UILabel* ptsLbl = [[UILabel alloc] initWithFrame:CGRectMake(annView.frame.origin.x, annView.frame.origin.y+8, 50, 20)];
//        ptsLbl.textAlignment = NSTextAlignmentCenter;
//        ptsLbl.text = @"test";
//        ptsLbl.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0];
//        [ptsLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10.0]];
//        if(annotation == [mapView1.annotations objectAtIndex:[mapView1.annotations indexOfObject:annotation]]){
//            NearByObject *nearObj1;
//            
//            if([mapView1.annotations indexOfObject:annotation] < 5){
//                pinButton.tag = [mapView1.annotations indexOfObject:annotation];
//                nearObj1 = (NearByObject*)[itemsArryNearBy objectAtIndex:[mapView1.annotations indexOfObject:annotation]];
//            }
//            else{
//                nearObj1 = (NearByObject*)[itemsArryNearBy objectAtIndex:[mapView1.annotations indexOfObject:annotation]-1];
//                pinButton.tag = [mapView1.annotations indexOfObject:annotation] - 1;
//            }
//            ptsLbl.text = [NSString stringWithFormat:@"$%@",nearObj1._currntRate];
//        }
//        
//        //[annView addSubview:ptsLbl];
//    }
//    
//    annView.annotation = annotation;
//    
//    
//    UIButton *pb = (UIButton *)[annView viewWithTag:10];
//    [pb setTitle:annotation.title forState:UIControlStateNormal];
//    
//    [Utility stopLoading:self.view];
//    return annView;
//    }
//    return nil;
//}

//-(void)mapPopUp:(id)sender withAnno:(MKAnnotationView*)av
//{
//    
//    UIButton* _btn = (UIButton*)sender;
//    //buttonIndex = _btn.tag;
//    
//    pinButton = sender;
//    annView = av;
//    
//	if(annView != nil)
//    {
//		myview.hidden = YES;
//		myview = nil;
//	}
//    NSLog(@"Its working");
//    
//    mapPopView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,300,300)];
//    
//    userLocLbl = [[UILabel alloc]initWithFrame:CGRectMake(40,10,220,40)];
//    [userLocLbl setFont:[UIFont boldSystemFontOfSize:16.0]];
//    
//    [userLocLbl setText:@"ArunSir"];
//    userLocLbl.numberOfLines = 2;
//    
//    myview = [[UIView alloc]initWithFrame:CGRectMake(-122,-270,300,300)];
//    closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [closeBtn addTarget:self
//                 action:@selector(closeBtn:)
//       forControlEvents:UIControlEventTouchDown];
//    
//    //[mapPopView setImage:[UIImage imageNamed:@"1.png"]];
//    [myview addSubview:userLocLbl];
//    [annView addSubview:myview];
//    [myview addSubview:mapPopView];
//    [myview sendSubviewToBack:mapPopView];
//    [annView addSubview:myview];
//}

-(void)closeBtn:(id)sender{
    
    if(annView != nil)
    {
		myview.hidden = YES;
		myview = nil;
	}
    
}


#pragma mark - Nearby API Call
-(void)apiCallNearByWithLat:(NSString*)latitude andLongi:(NSString*)lonitute withPage:(int)pgCnt{
    
    [Utility showLoadingIndicator:@"" withView:self.view];
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    [servConnectObj createConnectionRequestToURLWithHTTPS:[NSString stringWithFormat:@"https://api.park.com/Garages?page=%d",pgCnt] withAdditionalParameters:@""];

    
    
    
//    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
//    servConnectObj._delegate = self;
//    [servConnectObj createConnectionRequestToURLWithHTTPS:@"https://api.park.com/Garages" withAdditionalParameters:[NSString stringWithFormat:@"rpp=10&lat=%@&lon=%@&radius=1.5",latitude,lonitute]];
    
}

-(void)nearMeNowApiCallInBackground
{
    [Utility showLoadingIndicator:@"" withView:self.view];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        
        [self apiCallNearByWithLat:[NSString stringWithFormat:@"%f",40.80322647] andLongi:[NSString stringWithFormat:@"%f",-73.93453217] withPage:pageCnt];
        pageCnt++;
        // Perform the asynchronous task in this block like loading data from server
        
        // Perform the task on the main thread using the main queue-
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            
            // Perform the UI update in this block, like showing image.
            //itemsArryNearBy = [[NSMutableArray alloc]init];
            
            for (int i=0; i<[[responseDict valueForKey:@"Items"] count]; i++){
                NearByObject* nearObj = [NearByObject nearByFromDict:[[responseDict valueForKey:@"Items"] objectAtIndex:i]];
                [itemsArryNearBy addObject:nearObj];
            }
            
            if(pageCnt < 5)
                [self nearMeNowApiCallInBackground];
            else{
                [Utility stopLoading:self.view];
                [self mapViewDetails];
                [_tablViewList reloadData];
            }
        });
    });
}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    //[menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    //[navRightView setBackgroundColor:[UIColor redColor]];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 40, 34)];
    [searchButton setTitle:@"LIST" forState:UIControlStateNormal];
    [searchButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    //[searchButton setBackgroundColor:[UIColor blueColor]];
    //[searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(listBtnsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:searchButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationSearchBar];
    
    
}

-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)listBtnsClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    if (btn.tag == 0)
    {
        btn.tag = 1;
        [btn setTitle:@"MAP" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             btn.enabled = NO;
                             self._listView.hidden = NO;
                             [self._mapView addSubview:self._listView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self._mapView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             self._mapView.hidden = YES;
             [self.view addSubview:self._listView];
         }];
        [UIView commitAnimations];
        
    }
    else if (btn.tag == 1)
    {
        btn.tag = 0;
        [btn setTitle:@"LIST" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             btn.enabled = NO;
                             self._mapView.hidden = NO;
                             [self._listView addSubview:self._mapView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self._listView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             self._listView.hidden = YES;
             [self.view addSubview:self._mapView];
             
         }];
        [UIView commitAnimations];
        
    }
    
    
}

-(void)navigationSearchBar
{
    CGRect frame = CGRectMake(54,0,212,29);
    UIView* searchView = [[UIView alloc] initWithFrame:frame];
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 212, 29)];
    [imgView setImage:[UIImage imageNamed:@"header-search.png"]];
    [searchView addSubview:imgView];
    
    UITextField* searchTxtFled = [[UITextField alloc]initWithFrame:CGRectMake(25, 0, 185, 29)];
    [searchTxtFled setPlaceholder:@"Address, Postal Code, Airport Code"];
    [searchTxtFled setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [searchTxtFled setTextColor:[UIColor colorWithRed:38/255.0 green:106/255.0 blue:15/255.0 alpha:1.0]];
    searchTxtFled.delegate = self;
    [searchView addSubview:searchTxtFled];
    self.navigationItem.titleView = searchView;
    
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isSearching) {
        if (responseDict.count)
            return [responseDict count];
        else
            return 1;
    }
    else{
        if (filteredArray.count)
            return [filteredArray count];
        else
            return 1;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ParkListCell *cell = (ParkListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[ParkListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    else
        [cell prepareForReuse];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell._navigationBtn.tag = indexPath.row;
    [cell._navigationBtn addTarget:self action:@selector(navigationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!isSearching) {
        if (![itemsArryNearBy count]) {
            cell._detailView.hidden = YES;
            cell._noRecordsLbl.hidden = NO;
            return cell;
        }
        else{
            cell._detailView.hidden = NO;
            cell._noRecordsLbl.hidden = YES;
            
            NearByObject* nearByObj = (NearByObject*)[itemsArryNearBy objectAtIndex:indexPath.row];
            
            
            if(nearByObj._name != (id)[NSNull null])
                cell._garageNameLbl.text = nearByObj._name;
            
            if(nearByObj._address != (id)[NSNull null])
                cell._addressLbl.text = nearByObj._address;
            
            if((nearByObj._city != (id)[NSNull null]) && (nearByObj._state != (id)[NSNull null]) && (nearByObj._postal != (id)[NSNull null]))
                cell._cityStateZipLbl.text = [NSString stringWithFormat:@"%@, %@, %@",nearByObj._city,nearByObj._state,nearByObj._postal];
            
            if(nearByObj._phone != (id)[NSNull null]){
                NSMutableString *stringts = [NSMutableString stringWithString:nearByObj._phone];
                [stringts insertString:@"-" atIndex:3];
                [stringts insertString:@"-" atIndex:7];
                cell._phoneNoLbl.text = stringts;
            }
            
            if(nearByObj._type != (id)[NSNull null])
                cell._typeLbl.text = [NSString stringWithFormat:@"Type: %@",nearByObj._type];
            
            if(nearByObj._currntRate != (id)[NSNull null])
                [cell._btnCost setTitle:[NSString stringWithFormat:@"$%@",nearByObj._currntRate] forState:UIControlStateNormal];

            
//            cell._garageNameLbl.text = nearByObj._name;
//            cell._addressLbl.text = nearByObj._address;
//            cell._cityStateZipLbl.text = [NSString stringWithFormat:@"%@, %@, %@",nearByObj._city,nearByObj._state,nearByObj._postal];
//            
//            if(nearByObj._phone != (id)[NSNull null]){
//                NSMutableString *stringts = [NSMutableString stringWithString:nearByObj._phone];
//                [stringts insertString:@"-" atIndex:3];
//                [stringts insertString:@"-" atIndex:7];
//                cell._phoneNoLbl.text = stringts;
//            }
//            
//            
//            cell._typeLbl.text = [NSString stringWithFormat:@"Type: %@",nearByObj._type];
//            
//            [cell._btnCost setTitle:[NSString stringWithFormat:@"$%@",nearByObj._currntRate] forState:UIControlStateNormal];
        }
    }
    else{
        if (![filteredArray count]) {
            cell._detailView.hidden = YES;
            cell._noRecordsLbl.hidden = NO;
            return cell;
        }
        else{
            cell._detailView.hidden = NO;
            cell._noRecordsLbl.hidden = YES;
            
            
            if([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Name"] != (id)[NSNull null])
                cell._garageNameLbl.text = [[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Name"];
            
            if([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Address"] != (id)[NSNull null])
                cell._addressLbl.text = [[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Address"];
            
            if(([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"City"] != (id)[NSNull null]) && ([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"State"] != (id)[NSNull null]) && ([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Postal"] != (id)[NSNull null]))
                cell._cityStateZipLbl.text = [NSString stringWithFormat:@"%@, %@, %@",[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"City"],[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"State"],[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Postal"]];

            
            
            if([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Phone"] != (id)[NSNull null]){
                NSMutableString *stringts = [NSMutableString stringWithString:[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Phone"]];
                [stringts insertString:@"-" atIndex:3];
                [stringts insertString:@"-" atIndex:7];
                cell._phoneNoLbl.text = stringts;
            }
            
            
            if([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Type"] != (id)[NSNull null])
                cell._typeLbl.text = [NSString stringWithFormat:@"Type: %@",[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"Type"]];
            
            if([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"CurrentRate"] != (id)[NSNull null])
                [cell._btnCost setTitle:[NSString stringWithFormat:@"$%@",[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"CurrentRate"]] forState:UIControlStateNormal];
            
        }
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UIButton Actions
- (IBAction)filterClicked:(id)sender {
    FilterScreen* filtScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FilterScreen"];
    [self.navigationController pushViewController:filtScreen animated:YES];
}

-(void)navigationButtonClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    NavigationDecisionScreen* navDecPScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationDecisionScreen"];
    [self.navigationController pushViewController:navDecPScreen animated:YES];

    NearByObject* nearByObj;
    if (!isSearching){
        nearByObj = [itemsArryNearBy objectAtIndex:btn.tag];
        [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",nearByObj._address,nearByObj._city,nearByObj._state]withLatitute:nearByObj._latitude andLongitute:nearByObj._longitute];
    }
    else{
        nearByObj = [filteredArray objectAtIndex:btn.tag];
        [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",[[filteredArray objectAtIndex:btn.tag] valueForKey:@"Address"],[[filteredArray objectAtIndex:btn.tag] valueForKey:@"City"],[[filteredArray objectAtIndex:btn.tag] valueForKey:@"State"]]withLatitute:[[filteredArray objectAtIndex:btn.tag] valueForKey:@"Latitude"] andLongitute:[[filteredArray objectAtIndex:btn.tag] valueForKey:@"Longitude"]];
    }

}

/*
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    //    UIAlertView *errorAlert = [[UIAlertView alloc]
    //                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        NSLog(@"Longitute:%@ and Latitute:%@",[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude],[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
        
        // Stop Location Manager
        [locationManager stopUpdatingLocation];
    }
    
    
    // Reverse Geocoding
    //NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            NSLog(@"Address is:%@ %@\n%@ %@\n%@\n%@",
                  placemark.subThoroughfare, placemark.thoroughfare,
                  placemark.postalCode, placemark.locality,
                  placemark.administrativeArea,
                  placemark.country);
            //            addressLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
            //                                 placemark.subThoroughfare, placemark.thoroughfare,
            //                                 placemark.postalCode, placemark.locality,
            //                                 placemark.administrativeArea,
            //                                 placemark.country];
        }
        else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
}
*/


#pragma mark- ServerConnect Delegate
- (void)dataLoadRequestFailedWithError:(NSError*)error andMsg:(NSString*)errorMsg;
{
    [Utility stopLoading:self.view];
    NSLog(@"Error in ParkNearByScreen View Controller");
}
- (void)dictLoadedFromServer:(NSDictionary*)dict;
{
    [Utility stopLoading:self.view];
    responseDict = dict;
}

- (void)stringLoadedFromServer:(NSString*)str;
{
    
}
- (void)arrayLoadedFromServer:(NSArray*)Array;
{
    NSLog(@"Received array is %@",Array);
    
}

#pragma mark- UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    if(textField.tag == 0)
        [self textFieldDidChangeText:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"XTXT TAG %ld",(long)textField.tag);
    //if(textField.tag != 0 && textField.text.length > 0){
    //[self createComment:textField];
    //[self headerTapped:nil];
    //}
    
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidChangeText:(NSString*)textStr
{
    if(textStr.length > 0){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Name contains[c] %@", textStr];
        if(!self._listView.hidden || !self._mapView.hidden){
            filteredArray = [[responseDict valueForKey:@"Items"]   filteredArrayUsingPredicate:predicate];
            
        }
        isSearching = YES;
    }
    else
        isSearching = NO;
    
    [_tablViewList reloadData];
    
}


- (IBAction)pointerButtonTapped:(UIButton *)sender
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation, MAP_VIEW_REGION_DISTANCE, MAP_VIEW_REGION_DISTANCE);
    MKCoordinateRegion adjustedRegion = [self._mkMapView regionThatFits:region];
    [self._mkMapView setRegion:adjustedRegion animated:YES];
}


#pragma mark -
#pragma mark - Compare Image


- (BOOL)hp_isEqualToImage:(UIImage*)image
{
    NSData *data = [self hp_normalizedData:image];
    NSData *originalData = [self hp_normalizedData:[UIImage imageNamed:@"map-pin-restaurant.png"]];
    return [originalData isEqualToData:data];
}

- (NSData*)hp_normalizedData:(UIImage *)image
{
    const CGSize pixelSize = CGSizeMake(image.size.width * image.scale, image.size.height * image.scale);
    UIGraphicsBeginImageContext(pixelSize);
    [image drawInRect:CGRectMake(0, 0, pixelSize.width, pixelSize.height)];
    UIImage *drawnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return UIImagePNGRepresentation(drawnImage);
}



@end
