//
//  NearByCustomCell.m
//  Park.com
//
//  Created by Devendra Singh on 6/13/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "NearByCustomCell.h"

@implementation NearByCustomCell
@synthesize _addressLbl;
@synthesize _cityLbl;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
