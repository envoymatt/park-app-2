//
//  NearByCustomCell.h
//  Park.com
//
//  Created by Devendra Singh on 6/13/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearByCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *_addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *_cityLbl;

@end
