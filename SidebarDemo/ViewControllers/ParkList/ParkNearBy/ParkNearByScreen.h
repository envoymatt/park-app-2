//
//  ParkNearByScreen.h
//  Park
//
//  Created by Devendra Singh on 6/5/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


@interface ParkNearByScreen : UIViewController <CLLocationManagerDelegate,UITextFieldDelegate>

@end
