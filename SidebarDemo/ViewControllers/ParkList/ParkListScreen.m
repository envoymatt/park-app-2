//
//  ParkListScreen.m
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "ParkListScreen.h"
#import "ParkListCell.h"
#import "GarageView.h"
#import "Utility.h"

@interface ParkListScreen ()
{
    NSString* cellIdentifier;
}
@property (weak, nonatomic) IBOutlet UITableView *_parkListTableView;

@end

@implementation ParkListScreen
@synthesize _parkListTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    cellIdentifier = @"CellIdentifier";
    [_parkListTableView registerNib:[UINib nibWithNibName:@"ParkListCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ParkListCell *cell = (ParkListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[ParkListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    else
        [cell prepareForReuse];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell._noRecordsLbl.hidden = YES;
    
    return cell;
    
}
-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GarageView* gView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GarageView"];
    [self.navigationController pushViewController:gView animated:YES];
}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    [menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    //[navRightView setBackgroundColor:[UIColor redColor]];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 60, 34)];
    [searchButton setTitle:@"MAP" forState:UIControlStateNormal];
    [searchButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    //[searchButton setBackgroundColor:[UIColor blueColor]];
    //[searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(mapBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:searchButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationSearchBar];

    
}

-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)mapBtnClicked:(id)sender
{
    
}
-(void)navigationSearchBar
{
    CGRect frame = CGRectMake(54,0,212,29);
    UIView* searchView = [[UIView alloc] initWithFrame:frame];
    UITextField* searchTxtFled = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 212, 29)];
    [searchTxtFled setBackground:[UIImage imageNamed:@"header-search.png"]];
    [searchTxtFled setEnabled:NO];
    //[searchTxtFled setPlaceholder:@"New York, NY"];
    [searchView addSubview:searchTxtFled];
    self.navigationItem.titleView = searchView;
    
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}
-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"My Coupons";
    self.navigationItem.titleView = label;
    
}


@end
