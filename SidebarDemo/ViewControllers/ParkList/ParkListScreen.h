//
//  ParkListScreen.h
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkListScreen : UIViewController <UITableViewDelegate>

@end
