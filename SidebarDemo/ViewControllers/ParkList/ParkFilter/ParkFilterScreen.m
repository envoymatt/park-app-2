//
//  ParkFilterScreen.m
//  Park
//
//  Created by Devendra Singh on 6/4/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "ParkFilterScreen.h"
#import "Utility.h"
#import "NeighbourHoodScreen.h"

@interface ParkFilterScreen ()
@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;
- (IBAction)selectNeighbourClicked:(id)sender;


@end

@implementation ParkFilterScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self._scrollView setContentSize:CGSizeMake(320, 750)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 22, 22)];
    [backButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
    
//    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
//    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
//    [menuButton setImage:[UIImage imageNamed:@"header-back.png"] forState:UIControlStateNormal];
//    
    [backButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:backButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    //[navRightView setBackgroundColor:[UIColor redColor]];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 60, 34)];
    [searchButton setTitle:@"APPLY" forState:UIControlStateNormal];
    [searchButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    //[searchButton setBackgroundColor:[UIColor blueColor]];
    //[searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(applyBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:searchButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationSearchBar];
    
}

-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)applyBtnClicked:(id)sender
{
    
}
-(void)navigationSearchBar
{
    CGRect frame = CGRectMake(54,0,212,29);
    UIView* searchView = [[UIView alloc] initWithFrame:frame];
    UITextField* searchTxtFled = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 212, 29)];
    [searchTxtFled setBackground:[UIImage imageNamed:@"header-search.png"]];
    [searchTxtFled setEnabled:NO];
    //[searchTxtFled setPlaceholder:@"New York, NY"];
    [searchView addSubview:searchTxtFled];
    self.navigationItem.titleView = searchView;
    
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

- (IBAction)selectNeighbourClicked:(id)sender
{
    NeighbourHoodScreen* nhScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NeighbourHoodScreen"];
    [self.navigationController pushViewController:nhScreen animated:YES];
}


@end
