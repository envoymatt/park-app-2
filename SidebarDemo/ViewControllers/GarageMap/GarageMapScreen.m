//
//  GarageMapScreen.m
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "GarageMapScreen.h"
#import "Utility.h"
#import "SWRevealViewController.h"
#import "ParkListCell.h"
#import "ServerConnect.h"
#import "GarageView.h"
#import "Utility.h"
#import "NavigationDecisionScreen.h"
#import "GarageObject.h"
#import "AppDelegate.h"
#import "MyFavoriteScreen.h"
#import "NeighbourHoodScreen.h"
#import "CERangeSlider.h"
#import "SettingsViewController.h"


#define MAP_UPDATE_DISTANCE 200.00 // in meters
#define MAP_VIEW_REGION_DISTANCE 50.0 // in meters
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define NY_LATITUDE 40.714353
#define NY_LONGTITUDE -74.005973

@interface GarageMapScreen ()
{
    IBOutlet UIView *_listView;
    IBOutlet UIView *_mapView;
    IBOutlet UIView *_bottomTabView;
    
    IBOutlet UITextField* dateTxtFld1;
    IBOutlet UITextField* dateTxtFld2;
    IBOutlet UIView *_datePickerView;
    IBOutlet UIView *_filterBtnVw;
    
    IBOutlet UIView *_meteredView;
    IBOutlet UIView *_freeView;
    IBOutlet UIView *_favView;
    IBOutlet UIView *_filterView;
    IBOutlet UITableView *_tableViewList;
    IBOutlet UITableView *_tableViewFav;
    IBOutlet UILabel *_priceLbl;
    
    IBOutlet UIImageView* filterImgVw1;
    IBOutlet UIImageView* filterImgVw2;
    IBOutlet UIImageView* filterImgVw3;
    IBOutlet UIImageView* filterImgVw4;
    IBOutlet UIImageView* filterImgVw5;
    
    NSString* cellIdentifier;
    NSString* favCellIdentifier;
    
    BOOL isSearching;
    BOOL isdragMap;
    NSMutableArray* filterArr;
    NSMutableArray* itemArray;
    
    int pageCount;
    int searchPageCount;
    int dragPagCount;
    
    CLLocation* _location;
    AppDelegate* appDel;
    
    int datePckFlg;
    
    UITextField* searchTxtFled;
    
    NSDictionary* garageDict;
    
    UIView * navRightView;
    UIView*  searchView;
    
    CERangeSlider* _rangeSlider;
    MKCoordinateSpan _lastSpan;
    //CLLocationCoordinate2D userLocation;
    UIRefreshControl *refreshControl;
    
    
    
    double distanceBetCE;
    double distanceBetCW;
    double distanceBetCN;
    double distanceBetCS;
    BOOL _shouldRedrawAnnotation;
    BOOL isMapLoadFinshd;
    
    MKMapPoint centrePointRhb;
    CLLocationCoordinate2D _lastMapCenterCoordinate;
    MKAnnotationView* annotationView;
    ParkNeighbourhoodModel* parkModel;
    
    //NSInteger favBtnKey;    // To show selected and unselected btn in UITableView.
    
    
    //MKAnnotationView *annView;
    //int buttonIndex;
    //UIImageView* mapPopView;
    //UILabel* userLocLbl;
    //UIView* myview;
    //UIButton* closeBtn;
    //NSArray* searchArr;
    //NSMutableArray* searchArr;
    //BOOL mapFlg;
    // NSDictionary* searchDict;
    
    
    
}


@property (strong, nonatomic) IBOutlet MKMapView *_mkMapView;
@property (weak, nonatomic) IBOutlet UIScrollView *_scrollViewFilter;
@property (weak, nonatomic) IBOutlet MKMapView *_meteredMapView;
@property (weak, nonatomic) IBOutlet MKMapView *_freeMapView;
@property (weak, nonatomic) IBOutlet UIButton *_garageBtn;
@property (weak, nonatomic) IBOutlet UIButton *_meteredBtn;
@property (weak, nonatomic) IBOutlet UIButton *_freebtn;
@property (weak, nonatomic) IBOutlet UIButton *_favoriteBtn;
@property (weak, nonatomic) IBOutlet UIButton *_filterBtn;



- (IBAction)selectNeighbourClicked:(id)sender;
- (IBAction)garageBtnClicked:(id)sender;
- (IBAction)meteredBtnClicked:(id)sender;
- (IBAction)freeBtnClicked:(id)sender;
- (void)favoriteBtnListViewClicked:(id)sender;
- (IBAction)filteredBtnClicked:(id)sender;
- (IBAction)favBtnBottomClicked:(id)sender;
- (IBAction)favoriteTabBtnClicked:(id)sender;
@end

@implementation GarageMapScreen
@synthesize locationManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!_listView.hidden)
        [_tableViewList reloadData];
}

-(void)initSliderView
{
    CGRect sliderFrame = CGRectMake(35,175,250,10);
    _rangeSlider = [[CERangeSlider alloc] initWithFrame:sliderFrame];
    
    [self._scrollViewFilter addSubview:_rangeSlider];
    
    [_rangeSlider addTarget:self
                     action:@selector(slideValueChanged:)
           forControlEvents:UIControlEventValueChanged];
    
    [self performSelector:@selector(updateState) withObject:nil afterDelay:1.0f];
    
}

- (void)slideValueChanged:(id)control
{
    NSLog(@"Slider value changed: (%.2f,%.2f)",
          _rangeSlider.lowerValue, _rangeSlider.upperValue);
    
    _priceLbl.text = [NSString stringWithFormat:@"$%.2f to $%.2f per hour",_rangeSlider.lowerValue,_rangeSlider.upperValue];
}

- (void)updateState
{
    _rangeSlider.trackHighlightColour = [UIColor redColor];
    _rangeSlider.curvatiousness = 1.0;
}

- (void)gotoNewYorkLocation
{
    MKCoordinateRegion newRegion;
    
    newRegion.center.latitude = NY_LATITUDE;
    newRegion.center.longitude = NY_LONGTITUDE;
    
    newRegion.span.latitudeDelta = 0.2f;
    newRegion.span.longitudeDelta = 0.2f;
    
    [self._mkMapView setRegion:newRegion animated:YES];
    [self._meteredMapView setRegion:newRegion animated:YES];
    [self._freeMapView setRegion:newRegion animated:YES];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[Utility showLoadingIndicator:@"" withView:self.view];
    
    parkModel = [ParkNeighbourhoodModel sharedManager];
    parkModel.arraySelectedBtns = [[NSMutableArray alloc] init];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnMapToResignKeyBoard)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.delegate = self;
    [self._mkMapView addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMap:)];
    panGesture.delegate = self;
    [self._mkMapView addGestureRecognizer:panGesture];
    
    filterArr = [[NSMutableArray alloc]init];
    itemArray = [[NSMutableArray alloc]init];
    
    
    self._mkMapView.showsUserLocation = YES;
    
    pageCount = 0;
    searchPageCount = 0;
    
    //Pull to Refresh
    refreshControl = [[UIRefreshControl alloc] init];
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    // Configure View Controller
    [_tableViewList addSubview:refreshControl];
    //
    
    [self._scrollViewFilter setContentSize:CGSizeMake(320,765)];
    [self setupNavigationBar];
    _datePickerView.hidden = YES;
    
    [self garageBtnClicked:nil];
    [self initSliderView];
    
    
    // Create and initialize a tap gesture
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(pickerTapped:)];
    
    // Specify that the gesture must be a single tap
    tapRecognizer.numberOfTapsRequired = 1;
    // Add the tap gesture recognizer to the view
    [datePickerObj addGestureRecognizer:tapRecognizer];
    tapRecognizer.delegate = self;
    
    appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if (appDel.searchString)
        searchTxtFled.text = appDel.searchString;

    cellIdentifier = @"Cell";
    favCellIdentifier = @"Fav";
    
    [_tableViewFav registerNib:[UINib nibWithNibName:@"ParkListCell" bundle:nil] forCellReuseIdentifier:favCellIdentifier];
    
    [_tableViewList registerNib:[UINib nibWithNibName:@"ParkListCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];

    //[Utility showLoadingIndicator:@"" withView:self.view];
}

//- (void)appToBackground{
//    [self._mkMapView setShowsUserLocation:NO];
//}
//- (void)appReturnsActive{
//    [self._mkMapView setShowsUserLocation:YES];
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Garages API Call
- (void)apiCallGaragesWithLatitude:(float)lat longitute:(float)longi pageCount:(int)pgCnt withSender:(id)sender
{
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    [servConnectObj createConnectionRequestToURLWithHTTPS:[NSString stringWithFormat:@"https://api.park.com/Garages?page=%d&rpp=%d&lat=%f&lon=%f",pgCnt,25,lat,longi] withAdditionalParameters:@""];
}

- (void)searchApiCall:(int)sPgCnt WithSearchString:(NSString*)searchTxt
{
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    [servConnectObj createConnectionRequestToURLWithHTTPS:[NSString stringWithFormat:@"https://api.park.com/Garages?page=%d&rpp=%d&address=%@",sPgCnt,25,searchTxt] withAdditionalParameters:@""];
}

- (void)dragMapApiCallWithLatitude:(float)lati longitute:(float)longi andPageCount:(int)dragPgCnt
{
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    [servConnectObj createConnectionRequestToURLWithHTTPS:[NSString stringWithFormat:@"https://api.park.com/Garages?page=%d&rpp=%d&lat=%f&lon=%f",dragPagCount,25,lati,longi] withAdditionalParameters:@""];
}

- (void)garageDetailsInBackgroundWithLat:(float)lati andLong:(float)longi
{
    // Park-2#Changes
    [self setRecentDataKeyIntoModel];
    // @end

//    lati = 40.7127;
//    longi = -74.0059;
    [Utility showLoadingIndicator:@"" withView:self.view];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        pageCount++;
        [self apiCallGaragesWithLatitude:lati longitute:longi pageCount:pageCount withSender:nil];
        // Perform the asynchronous task in this block like loading data from server
        
        // Perform the task on the main thread using the main queue-
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            // Perform the UI update in this block, like showing image.
            [Utility stopLoading:self.view];
            
            if(self._mkMapView.annotations)
                [self._mkMapView removeAnnotations:self._mkMapView.annotations];
            
            NSMutableArray* dummyArr = [[NSMutableArray alloc]init];
            NSArray* listArr;
            
            for (int i=0; i<[[garageDict valueForKey:@"Items"] count]; i++){
                GarageObject* garObj = [GarageObject garagesFromDict:[[garageDict valueForKey:@"Items"] objectAtIndex:i]];
                [dummyArr addObject:garObj];
            }
            
            listArr = itemArray;
            filterArr = [[NSMutableArray alloc]init];
            itemArray = [[NSMutableArray alloc]init];
            
            
            for (int i=0; i<[dummyArr count]; i++){
                GarageObject* garObj = [dummyArr objectAtIndex:i];
                [filterArr addObject:garObj];
            }
            
            for (int i=0; i<[listArr count]; i++){
                GarageObject* garObj = [listArr objectAtIndex:i];
                [filterArr addObject:garObj];
            }
            itemArray = filterArr;
            [self mapViewDetails];
            [_tableViewList reloadData];
        });
    });
}

-(void)dragMapInBackGroundWithLat:(float)lat andLong:(float)longi
{
    [Utility showLoadingIndicator:@"" withView:self.view];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        dragPagCount++;
        [self dragMapApiCallWithLatitude:lat longitute:longi andPageCount:dragPagCount];
        // Perform the asynchronous task in this block like loading data from server
        
        // Perform the task on the main thread using the main queue-
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            // Perform the UI update in this block, like showing image.
            [Utility stopLoading:self.view];
            
            if(!_listView.hidden || !_mapView.hidden){
                if(self._mkMapView.annotations)
                    [self._mkMapView removeAnnotations:self._mkMapView.annotations];
                
                NSMutableArray* searchDummyArr = [[NSMutableArray alloc]init];
                NSArray* searchListArr;
                
                for (int i=0; i<[[garageDict valueForKey:@"Items"] count]; i++){
                    GarageObject* garObj = [GarageObject garagesFromDict:[[garageDict valueForKey:@"Items"] objectAtIndex:i]];
                    [searchDummyArr addObject:garObj];
                }
                
                searchListArr = itemArray;
                filterArr = [[NSMutableArray alloc]init];
                itemArray = [[NSMutableArray alloc]init];
                
                
                for (int i=0; i<[searchDummyArr count]; i++){
                    GarageObject* garObj = [searchDummyArr objectAtIndex:i];
                    [filterArr addObject:garObj];
                }
                
                for (int i=0; i<[searchListArr count]; i++){
                    GarageObject* garObj = [searchListArr objectAtIndex:i];
                    [filterArr addObject:garObj];
                }
                itemArray = filterArr;
                [self mapViewDetails];
                [_tableViewList reloadData];
                
            }
            else if(!_favView.hidden){
                [_tableViewFav reloadData];
                
            }
        });
    });
}


#pragma mark - MapView Delegate
-(void)mapViewDetails
{
    NSArray* mapArr;
    mapArr = filterArr;
    
    NSMutableArray* arr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<[mapArr count];i++)
    {
        GarageObject* garaObje = (GarageObject*)[mapArr objectAtIndex:i];
        _location = [[CLLocation alloc]initWithLatitude:[garaObje._latitude floatValue] longitude:[garaObje._longitute floatValue]];
        
        [arr addObject:_location];
    }
    [self annotationPinDetails:mapArr];
    
}

-(void)annotationPinDetails:(NSArray*)mapArray
{
    MKMapView* currentMap;
    currentMap.delegate = self;
    
    for(int i=0;i<[mapArray count];i++)
    {
        GarageObject* garObjs = (GarageObject*)[mapArray objectAtIndex:i];
        
        //NSLog(@"Add %@ \n Lat %@ \n Long %@ \n CRate %@",garObjs._address,garObjs._latitude,garObjs._longitute,garObjs._currntRate);
        
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake(0, 0);
        
        if(garObjs._latitude != nil && garObjs._latitude != (id)[NSNull null]){
            annotationCoord.latitude =  [garObjs._latitude floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        if(garObjs._longitute != nil && garObjs._longitute != (id)[NSNull null])
        {
            annotationCoord.longitude = [garObjs._longitute floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        
        
        if(!_meteredView.hidden){
            currentMap = self._meteredMapView;
            [self._meteredMapView addAnnotation:annotationPoint];
        }
        else if(!_freeView.hidden){
            currentMap = self._freeMapView;
            [self._freeMapView addAnnotation:annotationPoint];
        }
        else if(!_mapView.hidden || !_listView.hidden){
            currentMap = self._mkMapView;
            [self._mkMapView addAnnotation:annotationPoint];
        }
    }
    
    if(!_meteredView.hidden)
        [self zoomIn:self._meteredMapView];
    else if(!_freeView.hidden)
        [self zoomIn:self._freeMapView];
    else if(!_listView.hidden||!_mapView.hidden)
        [self zoomIn:self._mkMapView];
    
    
    NSLog(@"Annotation Count %lu",(unsigned long)[self._mkMapView.annotations count]);
    
    
}

- (void) handlePinButtonTap:(UITapGestureRecognizer *)gestureRecognizer
{
    UIButton *btn = (UIButton *) gestureRecognizer.view;
    NSLog(@"handlePinButtonTap: ann.title=%ld", (long)btn.tag);
    
    if(!_listView.hidden || !_mapView.hidden)
        [self navigateToListView:btn.tag];
    
    
}


- (void) showSettingScreen:(UITapGestureRecognizer *)gestureRecognizer {
    SettingsViewController* settingsViewObj = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:settingsViewObj animated:YES];
}

-(void)navigateToListView:(int)garageIndex
{
    GarageObject* garObject;
    garObject = (GarageObject*)[filterArr objectAtIndex:garageIndex];
    
    //    if (!isSearching) {
    //       garObject = (GarageObject*)[filterArr objectAtIndex:garageIndex];
    //    }
    //    else if (isSearching)
    //        garObject = (GarageObject*)[searchArr objectAtIndex:garageIndex];
    
    if (![appDel.recentVisitedArr containsObject:[filterArr objectAtIndex:garageIndex]]) {
        [appDel.recentVisitedArr addObject:[filterArr objectAtIndex:garageIndex]];
    }
    
    
    GarageView* gView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GarageView"];
    [self.navigationController pushViewController:gView animated:YES];
    [gView garageDetailId:garObject];
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    //NSLog(@"MAp view %d",view.tag);
    
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
    //[Utility showLoadingIndicator:@"" withView:self.view];
    //NSLog(@"Ann Pin %lu",(unsigned long)[mapView1.annotations indexOfObject:annotation]);
    
    NSArray* mapArray;
    mapArray = filterArr;
    
    static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
    if (annotation == self._mkMapView.userLocation){
        //MKAnnotationView* annotationView;
        //_lastMapCenterCoordinate = self._mkMapView.userLocation.coordinate;
		// We can return nil to let the MapView handle the default annotation view (blue dot):
		// return nil;
        
		// Or instead, we can create our own blue dot and even configure it:
        
		annotationView = [mapView1 dequeueReusableAnnotationViewWithIdentifier:@"blueDot"];
//        [annotationView setFrame:CGRectMake(annotationView.frame.origin.x+40, annotationView.frame.origin.y,30,30)];
		if (annotationView != nil) {
			annotationView.annotation = annotation;
		}
		else {
			annotationView = [[NSClassFromString(@"MKUserLocationView") alloc] initWithAnnotation:annotation reuseIdentifier:@"blueDot"];
		}
        ((MKUserLocation *)annotation).title = @"Valet/Pay";
        annotationView.image = [UIImage imageNamed:@"valet-pin.png"];
        //mypin.pinColor = MKPinAnnotationColorPurple;
        annotationView.backgroundColor = [UIColor clearColor];
        UIButton *goToDetail = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.rightCalloutAccessoryView = goToDetail;
        annotationView.draggable = NO;
        annotationView.highlighted = YES;
        annotationView.canShowCallout = YES;
        annotationView.annotation = annotation;
        return annotationView;
//        [annotationView setFrame:CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y,50,50)];
//        
//        UIImageView *pinButton = [[UIImageView alloc] init];
//        pinButton.frame = CGRectMake(-8,-48,70,50);
//        [pinButton setImage:[UIImage imageNamed:@"valet-bubble.png"]];
//        pinButton.userInteractionEnabled = YES;
//        
//        UIImageView *imgObj = [[UIImageView alloc] init];
//        imgObj.frame = CGRectMake(9,-12,22,35);
//        [imgObj setImage:[UIImage imageNamed:@"valet-pin.png"]];
//        imgObj.userInteractionEnabled = YES;
//        annotationView.canShowCallout = NO;
//        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                       initWithTarget:self action:@selector(showSettingScreen:)];
//        tap.numberOfTapsRequired = 1;
//        [imgObj addGestureRecognizer:tap];
//        [pinButton addGestureRecognizer:tap];
//        [annotationView addSubview:imgObj];
//        [annotationView addSubview:pinButton];
//        return annotationView;
    
	}
    else {
        NSLog(@"MAP %lu",(unsigned long)mapView1.annotations.count);
        
        annotationView = [mapView1 dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
        
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:kPinAnnotationIdentifier];
        
        [annotationView setFrame:CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y,50,50)];
        
        UIButton *pinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pinButton.frame = CGRectMake(0,0,50,50);
        GarageObject* garObj1;
        
        if(!_meteredView.hidden)
            [pinButton setBackgroundImage:[UIImage imageNamed:@"map-pin4.png"] forState:UIControlStateNormal];
        else if(!_freeView.hidden)
            [pinButton setBackgroundImage:[UIImage imageNamed:@"map-pin2.png"] forState:UIControlStateNormal];
        else if(!_listView.hidden || !_mapView.hidden)
            [pinButton setBackgroundImage:[UIImage imageNamed:@"map-pin3.png"] forState:UIControlStateNormal];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handlePinButtonTap:)];
        tap.numberOfTapsRequired = 1;
        [pinButton addGestureRecognizer:tap];
        [annotationView addSubview:pinButton];
        
        UILabel* ptsLbl = [[UILabel alloc] initWithFrame:CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y+8, 50, 20)];
        ptsLbl.textAlignment = NSTextAlignmentCenter;
        ptsLbl.backgroundColor = [UIColor clearColor];
        ptsLbl.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0];
        [ptsLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10.0]];
        
        if(annotation == [mapView1.annotations objectAtIndex:[mapView1.annotations indexOfObject:annotation]]){
            
            if([mapView1.annotations indexOfObject:annotation] < [mapArray count]){
                pinButton.tag = [mapView1.annotations indexOfObject:annotation];
                garObj1 = (GarageObject*)[mapArray objectAtIndex:[mapView1.annotations indexOfObject:annotation]];
            }
            else{
                garObj1 = (GarageObject*)[mapArray objectAtIndex:[mapView1.annotations indexOfObject:annotation]- 1];
                pinButton.tag = [mapView1.annotations indexOfObject:annotation] - 1;
            }
            
            if (!_meteredView.hidden)
                ptsLbl.text = [NSString stringWithFormat:@"$%@",garObj1._currntRate];
            else if (!_freeView.hidden){
                UIImageView* imgView = [[UIImageView alloc]initWithFrame:CGRectMake(annotationView.frame.origin.x+15, annotationView.frame.origin.y+9, 21, 21)];
                imgView.image = [UIImage imageNamed:@"freeIcon.png"];
                [annotationView addSubview:imgView];
            }
            else if (!_listView.hidden || !_mapView.hidden)
                ptsLbl.text = [NSString stringWithFormat:@"$%@",garObj1._currntRate];
            
            [annotationView addSubview:ptsLbl];
            annotationView.annotation = annotation;
        }
        UIButton *pb = (UIButton *)[annotationView viewWithTag:10];
        [pb setTitle:annotation.title forState:UIControlStateNormal];
        
        return annotationView;
    }
    return nil;
}

-(void)zoomIn:(MKMapView*)mapView
{
    MKCoordinateRegion region;
    
    if ([mapView.annotations count]) {
        
        float minLng = 0;
        float maxLng = 0;
        float minLat = 0;
        float maxLat = 0;
        
        for ( ParkAnnotation *annotation in mapView.annotations )
        {
            if ( minLat == 0 || annotation.coordinate.latitude < minLat )
                minLat = annotation.coordinate.latitude;
            if ( maxLat == 0 || annotation.coordinate.latitude > maxLat )
                maxLat = annotation.coordinate.latitude;
            
            if ( minLng == 0 || annotation.coordinate.longitude < minLng )
                minLng = annotation.coordinate.longitude;
            if ( maxLng == 0 || annotation.coordinate.longitude > maxLng )
                maxLng = annotation.coordinate.longitude;
        }
        
        float mapPadding = 1.2;
        float minVisLat = 0.01;
        
        region.center.latitude = (minLat + maxLat) / 2;
        region.center.longitude = (minLng + maxLng) / 2;
        region.span.latitudeDelta = (maxLat - minLat) * mapPadding;
        region.span.latitudeDelta = (region.span.latitudeDelta < minVisLat) ? minVisLat : region.span.latitudeDelta;
        region.span.longitudeDelta = (maxLng - minLng) * mapPadding;
        
        _lastSpan = region.span;
        [mapView setRegion:region animated:NO];
        
    }
    
    else {
        //        region.center.latitude = NY_LATITUDE;
        //        region.center.longitude = NY_LONGTITUDE;
        //        region.span.latitudeDelta = 0.5f;
        //        region.span.longitudeDelta = 0.5f;
    }
    
    
}


#pragma mark - Drag Map

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    
    if (_shouldRedrawAnnotation)
    {
        CGPoint annotationPoint = [mapView convertCoordinate:annotationView.annotation.coordinate
                                               toPointToView:mapView];
        
        [mapView selectAnnotation:annotationView.annotation animated:YES];
        
        if (annotationPoint.x < 25)
        {
            _shouldRedrawAnnotation = YES;
        }
        else
        {
            _shouldRedrawAnnotation = NO;
        }
    }
    
    
    MKMapRect mRect = __mkMapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    NSLog(@"eastpoint x and y \n %f \n %f",eastMapPoint.x,eastMapPoint.y);
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint northMapPoint = MKMapPointMake(MKMapRectGetMidX(mRect), MKMapRectGetMinY(mRect));
    MKMapPoint southMapPoint = MKMapPointMake(MKMapRectGetMidX(mRect), MKMapRectGetMaxY(mRect));
    
    MKMapPoint newCenterMapPoint = MKMapPointMake(MKMapRectGetMidX(mRect), MKMapRectGetMidY(mRect));
    
    distanceBetCE = MKMetersBetweenMapPoints(centrePointRhb, eastMapPoint);
    distanceBetCW = MKMetersBetweenMapPoints(centrePointRhb, westMapPoint);
    distanceBetCN = MKMetersBetweenMapPoints(centrePointRhb, northMapPoint);
    distanceBetCS = MKMetersBetweenMapPoints(centrePointRhb, southMapPoint);
    
    CLLocationDistance newDistanceBetCE = MKMetersBetweenMapPoints(newCenterMapPoint, eastMapPoint);
    CLLocationDistance newDistanceBetCW = MKMetersBetweenMapPoints(newCenterMapPoint, westMapPoint);
    CLLocationDistance newDistanceBetCN = MKMetersBetweenMapPoints(newCenterMapPoint, northMapPoint);
    CLLocationDistance newDistanceBetCS = MKMetersBetweenMapPoints(newCenterMapPoint, southMapPoint);
    
    double diffBetNewCE = distanceBetCE-newDistanceBetCE;
    double diffBetNewCW = distanceBetCW-newDistanceBetCW;
    double diffBetNewCN = distanceBetCN-newDistanceBetCN;
    double diffBetNewCS = distanceBetCS-newDistanceBetCS;
    
    
    MKCoordinateRegion mapRegion;
    
    mapRegion.center = mapView.centerCoordinate;
    
    double latitude = mapRegion.center.latitude;
    double longitude = mapRegion.center.longitude;
    CLLocation* lastLocation = [[CLLocation alloc] initWithLatitude:_lastMapCenterCoordinate.latitude longitude:_lastMapCenterCoordinate.longitude];
    
    
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:latitude
                                                             longitude:longitude];
    
    CLLocationDistance distance = ([lastLocation distanceFromLocation:currentLocation]);
    
    if (diffBetNewCE > newDistanceBetCE*.70 || diffBetNewCW > newDistanceBetCW*.70 || diffBetNewCN > newDistanceBetCN*.70 || diffBetNewCS > newDistanceBetCS*.70)
    {
        NSLog(@"Plz hit API");
        centrePointRhb = newCenterMapPoint;
        isMapLoadFinshd = NO;
        if (appDel.isHitAPI){
            [filterArr removeAllObjects];
            NSLog(@"here it is %f and %f",mapView.centerCoordinate.latitude,mapView.centerCoordinate.longitude);
            self._mkMapView.showsUserLocation = YES;
            [self dragMapInBackGroundWithLat:mapView.centerCoordinate.latitude andLong:mapView.centerCoordinate.longitude];
            
            appDel.isHitAPI = NO;
        }
    }
    else
    {
        NSLog(@"DON'T HIT API");
        isMapLoadFinshd = YES;
    }
    
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    if (!isMapLoadFinshd)
    {
        MKMapRect mRect = self._mkMapView.visibleMapRect;
        centrePointRhb = MKMapPointMake(MKMapRectGetMidX(mRect), MKMapRectGetMidY(mRect));
        //NSLog(@"*********#$@$@#$@#$@#$@#$@#$ not hit");
    }
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.centerCoordinate;
    
    _lastMapCenterCoordinate.latitude = mapRegion.center.latitude;
    _lastMapCenterCoordinate.longitude = mapRegion.center.longitude;
    
    
}

#pragma mark - GestureRecognizer
-(void)dragMap:(UIPanGestureRecognizer*)gesture{
    if (gesture.state == UIGestureRecognizerStateEnded){
        [parkModel.arraySelectedBtns removeAllObjects];
        NSLog(@"drag ended");
        
        appDel.isHitAPI = YES;
        dragPagCount = 0;
        searchTxtFled.text = nil;
        isSearching = NO;
        isdragMap = YES;
        
        _rangeSlider.lowerValue = 0;
        _rangeSlider.upperValue = 20;
        _priceLbl.text = [NSString stringWithFormat:@"$%.2f to $%.2f per hour",_rangeSlider.lowerValue,_rangeSlider.upperValue];
        
    }
    
    
}

-(void)tapOnMapToResignKeyBoard
{
    [searchTxtFled resignFirstResponder];
}

#pragma mark - Alert View Delegates
-(void)showAlertMessage:(NSString*)msg
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"B&W App!!" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}



#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self changeStatusBarColor];
    
    [self navigationLeftSideView];
    
    [self navigationRightSideView];
    
    [self navigationSearchBar];
}

-(void)navigationLeftSideView
{
    UIView* navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
}

-(void)navigationFilterLeftSideView
{
    UIView* navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 50, 29)];
    [menuButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [menuButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    
    [menuButton addTarget:self action:@selector(filterCancelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
}

-(void)navigationRightSideView
{
    navRightView = [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    UIButton* listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(0, 0, 40, 34)];
    [listButton setTitle:@"LIST" forState:UIControlStateNormal];
    [listButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    
    
    if(!_listView.hidden){
        [listButton addTarget:self action:@selector(listBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(!_favView.hidden){
        [listButton addTarget:self action:@selector(filterBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    [navRightView addSubview:listButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
}

-(void)navigationFilterRightSideView
{
    navRightView = [[UIView alloc]initWithFrame:CGRectMake(220, 0, 40, 34)];
    UIButton* listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(0, 0, 40, 34)];
    [listButton setTitle:@"Apply" forState:UIControlStateNormal];
    [listButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    
    [listButton addTarget:self action:@selector(filterApplyBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:listButton];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
}


-(void)filterCancelBtnClicked:(id)sender{
    
    [parkModel.arraySelectedBtns removeAllObjects];
    _rangeSlider.lowerValue = 0;
    _rangeSlider.upperValue = 20;
    _priceLbl.text = [NSString stringWithFormat:@"$%.2f to $%.2f per hour",_rangeSlider.lowerValue,_rangeSlider.upperValue];
    _mapView.hidden = NO;
    
    filterArr = itemArray;
    
    [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    [_tableViewList reloadData];
    [self mapViewDetails];
    [self garageBtnClicked:nil];
}


-(void)filterApplyBtnClicked:(id)sender{
    NSLog(@"Apply");
    NSLog(@"T1 %@ \n T2 %@ \n Price %@",dateTxtFld1.text,dateTxtFld2.text,_priceLbl.text);
    
    filterArr = [[NSMutableArray alloc]init];
    
    NSMutableArray* priceFilterArry = [[NSMutableArray alloc] init];
    NSMutableArray* neighoodFilterArry = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[itemArray count]; i++){
        GarageObject* garObj = [itemArray objectAtIndex:i];
        
        if([garObj._currntRate floatValue] >= _rangeSlider.lowerValue && [garObj._currntRate floatValue] <= _rangeSlider.upperValue){
            [priceFilterArry addObject:garObj];
        }
    }
    
    ParkNeighbourhoodModel* pNModal = [ParkNeighbourhoodModel sharedManager];
    if(self._mkMapView.annotations)
        [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    
    for (int i=0; i < pNModal.arrayAfterFilteration.count; i++){
        GarageObject* garObj = [GarageObject garagesFromDict:[pNModal.arrayAfterFilteration objectAtIndex:i]];
        [neighoodFilterArry addObject:garObj];
    }
    
    
    
    
    NSMutableArray* neighoodID = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < neighoodFilterArry.count; i++) {
        [neighoodID addObject:[[neighoodFilterArry objectAtIndex:i] valueForKey:@"_neigbouhoodId"]];
    }
    
    NSArray* neighHoodIDCount = [[NSSet setWithArray: neighoodID] allObjects];
    
    NSMutableArray* dummyArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < neighHoodIDCount.count; i++) {
        NSLog(@"primary: %@",neighHoodIDCount[i]);
        for (int j = 0; j < priceFilterArry.count; j++) {
            GarageObject* garajObjSec = (GarageObject*)priceFilterArry[j];
            NSLog(@"secondray: %@",garajObjSec._neigbouhoodId);
            if ([neighHoodIDCount[i] intValue] == [garajObjSec._neigbouhoodId intValue])
                [dummyArr addObject:priceFilterArry[j]];
        }
    }
    //NSLog(@"%@",dummyArr);
    filterArr = dummyArr;
    
    
    _mapView.hidden = NO;
    if(self._mkMapView.annotations)
        [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    [self mapViewDetails];
    [_tableViewList reloadData];
    [self garageBtnClicked:nil];
    
    
    
}

#pragma mark - Neighbour Filter Apply
-(void)neighbourhoodFilterApply
{
    
    _listView.hidden = NO;
    _mapView.hidden = NO;
    [filterArr removeAllObjects];
    
    ParkNeighbourhoodModel* pNModal = [ParkNeighbourhoodModel sharedManager];
    if(self._mkMapView.annotations)
        [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    
    for (int i=0; i < pNModal.arrayAfterFilteration.count; i++){
        GarageObject* garObj = [GarageObject garagesFromDict:[pNModal.arrayAfterFilteration objectAtIndex:i]];
        [filterArr addObject:garObj];
    }
    
    [self mapViewDetails];
    [_tableViewList reloadData];
    
}


-(void)compareDate
{
    NSString *time1 = @"08:15:12";
    NSString *time2 = @"18:12:08";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *date1= [formatter dateFromString:time1];
    NSDate *date2 = [formatter dateFromString:time2];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
    }
    else if(result == NSOrderedAscending)
    {
        NSLog(@"date2 is later than date1");
    }
    else
    {
        NSLog(@"date1 is equal to date2");
    }
}


-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)listBtnClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0)
    {
        btn.tag = 1;
        [btn setTitle:@"MAP" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             _listView.hidden = NO;
                             btn.enabled = NO;
                             [_mapView addSubview:_listView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_mapView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             _mapView.hidden = YES;
             [self.view addSubview:_listView];
             [self.view bringSubviewToFront:_bottomTabView];
         }];
        [UIView commitAnimations];
        
    }
    else if (btn.tag == 1)
    {
        btn.tag = 0;
        [btn setTitle:@"LIST" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             btn.enabled = NO;
                             _mapView.hidden = NO;
                             [_listView addSubview:_mapView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_listView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             _listView.hidden = YES;
             [self.view addSubview:_mapView];
             [self.view bringSubviewToFront:_bottomTabView];
         }];
        [UIView commitAnimations];
        
    }
    
}

-(void)filterBtnClicked:(id)sender
{
    
    UIButton* btn = (UIButton*)sender;
    if (btn.tag == 0)
    {
        btn.tag = 1;
        [btn setTitle:@"MAP" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             _favView.hidden = NO;
                             btn.enabled = NO;
                             [_mapView addSubview:_favView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_mapView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             _mapView.hidden = YES;
             [self.view addSubview:_favView];
             [self.view bringSubviewToFront:_bottomTabView];
         }];
        [UIView commitAnimations];
        
    }
    else if (btn.tag == 1)
    {
        btn.tag = 0;
        [btn setTitle:@"LIST" forState:UIControlStateNormal];
        [UIView animateWithDuration:1
                         animations:^{
                             btn.enabled = NO;
                             _mapView.hidden = NO;
                             [_favView addSubview:_mapView];
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                             [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_favView cache:YES];
                         }
                         completion:^(BOOL finished)
         {
             btn.enabled = YES;
             _favView.hidden = YES;
             [self.view addSubview:_mapView];
             [self.view bringSubviewToFront:_bottomTabView];
         }];
        [UIView commitAnimations];
        
    }
    
}


-(void)navigationSearchBar
{
    CGRect frame = CGRectMake(54,0,212,29);
    searchView = [[UIView alloc] initWithFrame:frame];
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 212, 29)];
    [imgView setImage:[UIImage imageNamed:@"header-search.png"]];
    [searchView addSubview:imgView];
    
    searchTxtFled = [[UITextField alloc]initWithFrame:CGRectMake(25, 0, 182, 29)];
    searchTxtFled.delegate = self;
    searchTxtFled.clearButtonMode = UITextFieldViewModeAlways;

    [searchTxtFled setReturnKeyType:UIReturnKeySearch];
    [searchTxtFled setPlaceholder:@"Address, Postal Code, Airport Code"];
    [searchTxtFled setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    searchTxtFled.textColor = [UIColor whiteColor];
    [searchTxtFled setTextColor:[UIColor colorWithRed:38/255.0 green:106/255.0 blue:15/255.0 alpha:1.0]];
    [searchView addSubview:searchTxtFled];
    self.navigationItem.titleView = searchView;
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        //imageResize = [Utility imageWithImage:[UIImage imageNamed:@"navig_black_bar.png"] scaledToSize:CGSizeMake(320,64)];

        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];

        //imageResize = [Utility imageWithImage:[UIImage imageNamed:@"navig_black_bar.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - UITableView Methods
- (void)refresh:(id)sender {
    [parkModel.arraySelectedBtns removeAllObjects];
    _rangeSlider.lowerValue = 0;
    _rangeSlider.upperValue = 20;
    _priceLbl.text = [NSString stringWithFormat:@"$%.2f to $%.2f per hour",_rangeSlider.lowerValue,_rangeSlider.upperValue];
    
    if (isSearching)
        [self searchBackgroundWithSearchText:searchTxtFled.text];
    else if(isdragMap)
        [self dragMapInBackGroundWithLat:self._mkMapView.centerCoordinate.latitude andLong:self._mkMapView.centerCoordinate.longitude];
    else
        [self garageDetailsInBackgroundWithLat:[[NSUserDefaults standardUserDefaults] doubleForKey:@""] andLong:[[NSUserDefaults standardUserDefaults] doubleForKey:@""]];
    //[self garageDetailsInBackgroundWithLat:appDel.locationManager.location.coordinate.latitude andLong:appDel.locationManager.location.coordinate.longitude];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _tableViewList){
        
        if(filterArr.count)
            return [filterArr count];
        else
            return 1;
        
        //        if(!isSearching){
        //            if(filterArr.count)
        //                return [filterArr count];
        //            else
        //                return 1;
        //        }
        //        else{
        //            if(searchArr.count)
        //                return [searchArr count];
        //            else
        //                return 1;
        //        }
    }
    else if(tableView == _tableViewFav){
        
        if(appDel.favGarageArr.count)
            return [appDel.favGarageArr count];
        else
            return 1;
        //        if(!isSearching){
        //            if(appDel.favGarageArr.count)
        //                return [appDel.favGarageArr count];
        //            else
        //                return 1;
        //        }
        //        else{
        //            if(searchArr.count)
        //                return [searchArr count];
        //            else
        //                return 1;
        //        }
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    GarageObject *garObj;
    
    if(tableView == _tableViewList){
        
        ParkListCell *cell = (ParkListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil){
            cell = [[ParkListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell._navigationBtn.tag = indexPath.row;
        [cell._navigationBtn addTarget:self action:@selector(navigationBtnCliced:) forControlEvents:UIControlEventTouchUpInside];
        
        if(![filterArr count]){
            cell._detailView.hidden = YES;
            cell._noRecordsLbl.hidden = NO;
            return cell;
        }
        else{
            garObj = (GarageObject*)[filterArr objectAtIndex:indexPath.row];
            
            [self parkListCell:cell withGarageData:garObj andIndexPath:indexPath];
        }
        
        return cell;
    }
    else if(tableView == _tableViewFav){
        
        ParkListCell *cell = (ParkListCell*)[tableView dequeueReusableCellWithIdentifier:favCellIdentifier];
        
        if (cell == nil){
            cell = [[ParkListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:favCellIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell._navigationBtn.tag = indexPath.row;
        [cell._navigationBtn addTarget:self action:@selector(navigationBtnCliced:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(![appDel.favGarageArr count]){
            cell._detailView.hidden = YES;
            cell._noRecordsLbl.hidden = NO;
            return cell;
        }
        else{
            garObj = (GarageObject*)[appDel.favGarageArr objectAtIndex:indexPath.row];
            
            [self parkListCell:cell withGarageData:garObj andIndexPath:indexPath];
        }
        
        return cell;
    }
    return nil;
    
}

-(void)parkListCell:(ParkListCell*)cell withGarageData:(GarageObject*)garObj andIndexPath:(NSIndexPath *)indexPath{
    cell._detailView.hidden = NO;
    cell._noRecordsLbl.hidden = YES;
    
    if(garObj._name != (id)[NSNull null])
        cell._garageNameLbl.text = garObj._name;
    
    if(garObj._address != (id)[NSNull null])
        cell._addressLbl.text = garObj._address;
    
    if((garObj._city != (id)[NSNull null]) && (garObj._state != (id)[NSNull null]) && (garObj._postal != (id)[NSNull null]))
        cell._cityStateZipLbl.text = [NSString stringWithFormat:@"%@, %@, %@",garObj._city,garObj._state,garObj._postal];
    
    if(garObj._phone != (id)[NSNull null]){
        NSMutableString *stringts = [NSMutableString stringWithString:garObj._phone];
        [stringts insertString:@"-" atIndex:3];
        [stringts insertString:@"-" atIndex:7];
        cell._phoneNoLbl.text = stringts;
    }
    
    if(garObj._type != (id)[NSNull null])
        cell._typeLbl.text = [NSString stringWithFormat:@"Type: %@",garObj._type];
    
    if(garObj._currntRate != (id)[NSNull null])
        [cell._btnCost setTitle:[NSString stringWithFormat:@"$%@",garObj._currntRate] forState:UIControlStateNormal];
    
    cell._favBtn.tag = indexPath.row;
    
    [cell._favBtn addTarget:self action:@selector(favoriteBtnListViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(!garObj.favKey)
        [cell._favBtn setImage:[UIImage imageNamed:@"btn-favorite1-0"] forState:UIControlStateNormal];
    else
        [cell._favBtn setImage:[UIImage imageNamed:@"btn-favorite1-1"] forState:UIControlStateNormal];
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self navigateToListView:indexPath.row];
    
}


#pragma mark - UIButton Actions(Delegate)
- (IBAction)favBtnBottomClicked:(id)sender{
    
    [self._garageBtn setBackgroundImage:[UIImage imageNamed:@"garageIcon.png"] forState:UIControlStateNormal];
    [self._meteredBtn setBackgroundImage:[UIImage imageNamed:@"metered.png"] forState:UIControlStateNormal];
    [self._freebtn setBackgroundImage:[UIImage imageNamed:@"free.png"] forState:UIControlStateNormal];
    [self._favoriteBtn setBackgroundImage:[UIImage imageNamed:@"favoritesselected.png"] forState:UIControlStateNormal];
    [self._filterBtn setBackgroundImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    
    _listView.hidden = YES;
    _mapView.hidden = YES;
    _meteredView.hidden = YES;
    _freeView.hidden = YES;
    _favView.hidden = NO;
    _filterView.hidden = YES;
    searchTxtFled.text = @"";
    //isSearching = NO;
    [_tableViewFav reloadData];
    navRightView.hidden = YES;
    searchView.hidden = NO;
    [self navigationLeftSideView];
    [self.view bringSubviewToFront:_bottomTabView];
    
    
    
}
- (IBAction)garageBtnClicked:(id)sender{
    
    
    [self._garageBtn setBackgroundImage:[UIImage imageNamed:@"garageIconSelect.png"] forState:UIControlStateNormal];
    [self._meteredBtn setBackgroundImage:[UIImage imageNamed:@"metered.png"] forState:UIControlStateNormal];
    [self._freebtn setBackgroundImage:[UIImage imageNamed:@"free.png"] forState:UIControlStateNormal];
    [self._favoriteBtn setBackgroundImage:[UIImage imageNamed:@"favorites.png"] forState:UIControlStateNormal];
    [self._filterBtn setBackgroundImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    
    
    _listView.hidden = NO;
    _mapView.hidden = NO;
    _meteredView.hidden = YES;
    _freeView.hidden = YES;
    _favView.hidden = YES;
    _filterView.hidden = YES;
    searchTxtFled.text = appDel.searchString;
    //isSearching = NO;
    [_tableViewList reloadData];
    [self navigationRightSideView];
    [self navigationLeftSideView];
    [self hideNavigationView:NO];
    
}

- (IBAction)meteredBtnClicked:(id)sender{
    
    
    [self._garageBtn setBackgroundImage:[UIImage imageNamed:@"garageIcon.png"] forState:UIControlStateNormal];
    [self._meteredBtn setBackgroundImage:[UIImage imageNamed:@"meteredselected.png"] forState:UIControlStateNormal];
    [self._freebtn setBackgroundImage:[UIImage imageNamed:@"free.png"] forState:UIControlStateNormal];
    [self._favoriteBtn setBackgroundImage:[UIImage imageNamed:@"favorites.png"] forState:UIControlStateNormal];
    [self._filterBtn setBackgroundImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    
    _listView.hidden = YES;
    _mapView.hidden = YES;
    _meteredView.hidden = NO;
    _freeView.hidden = YES;
    _favView.hidden = YES;
    _filterView.hidden = YES;
    
    [self hideNavigationView:YES];
    [self navigationLeftSideView];
    [self.view bringSubviewToFront:_bottomTabView];
    
    UIButton* mtrBtn = (UIButton*)sender;
    if(mtrBtn.tag == 0){
        [self mapViewDetails];
        mtrBtn.tag = 1;
    }
    
}

- (IBAction)freeBtnClicked:(id)sender{
    
    [self._garageBtn setBackgroundImage:[UIImage imageNamed:@"garageIcon.png"] forState:UIControlStateNormal];
    [self._meteredBtn setBackgroundImage:[UIImage imageNamed:@"metered.png"] forState:UIControlStateNormal];
    [self._freebtn setBackgroundImage:[UIImage imageNamed:@"freeselected.png"] forState:UIControlStateNormal];
    [self._favoriteBtn setBackgroundImage:[UIImage imageNamed:@"favorites.png"] forState:UIControlStateNormal];
    [self._filterBtn setBackgroundImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    
    _listView.hidden = YES;
    _mapView.hidden = YES;
    _meteredView.hidden = YES;
    _freeView.hidden = NO;
    _favView.hidden = YES;
    _filterView.hidden = YES;
    [self navigationLeftSideView];
    [self hideNavigationView:YES];
    
    UIButton* freeBtn = (UIButton*)sender;
    if(freeBtn.tag == 0){
        [self mapViewDetails];
        freeBtn.tag = 1;
    }
    
}

- (IBAction)filteredBtnClicked:(id)sender{
    
    [self._garageBtn setBackgroundImage:[UIImage imageNamed:@"garageIcon.png"] forState:UIControlStateNormal];
    [self._meteredBtn setBackgroundImage:[UIImage imageNamed:@"metered.png"] forState:UIControlStateNormal];
    [self._freebtn setBackgroundImage:[UIImage imageNamed:@"free.png"] forState:UIControlStateNormal];
    [self._favoriteBtn setBackgroundImage:[UIImage imageNamed:@"favorites.png"] forState:UIControlStateNormal];
    [self._filterBtn setBackgroundImage:[UIImage imageNamed:@"filterselected.png"] forState:UIControlStateNormal];
    
    
    _listView.hidden = YES;
    _mapView.hidden = YES;
    _meteredView.hidden = YES;
    _freeView.hidden = YES;
    _favView.hidden = YES;
    _filterView.hidden = NO;
    searchView.hidden = YES;
    [self.view bringSubviewToFront:_bottomTabView];
    [self navigationFilterRightSideView];
    [self navigationFilterLeftSideView];
    
}


- (void)favoriteBtnListViewClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    NSLog(@"Button selected %d  btn tag %ld",btn.selected,(long)btn.tag);
    
    if(!_listView.hidden){
        GarageObject* garObj = [filterArr objectAtIndex:btn.tag];
        
        if (!garObj.favKey) {
            if (![appDel.favGarageArr containsObject:[filterArr objectAtIndex:btn.tag]]) {
                [appDel.favGarageArr addObject:[filterArr objectAtIndex:btn.tag]];
            }
            garObj.favKey = YES;
            [btn setImage:[UIImage imageNamed:@"btn-favorite1-1"] forState:UIControlStateNormal];
        }
        else{
            if ([appDel.favGarageArr containsObject:[filterArr objectAtIndex:btn.tag]]) {
                [appDel.favGarageArr removeObject:[filterArr objectAtIndex:btn.tag]];
            }
            garObj.favKey = NO;
            [btn setImage:[UIImage imageNamed:@"btn-favorite1-0"] forState:UIControlStateNormal];
        }
        [_tableViewList reloadData];
    }
    else if(!_favView.hidden){
        GarageObject* garObj = [appDel.favGarageArr objectAtIndex:btn.tag];
        
        if (garObj.favKey) {
            [appDel.favGarageArr removeObjectAtIndex:btn.tag];
            garObj.favKey = NO;
            [btn setImage:[UIImage imageNamed:@"btn-favorite1-0"] forState:UIControlStateNormal];
        }
        [_tableViewFav reloadData];
    }
}

-(void)navigationBtnCliced:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    GarageObject* garObject;
    
    NavigationDecisionScreen* navDecPScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationDecisionScreen"];
    [self.navigationController pushViewController:navDecPScreen animated:YES];
    
    
    if(!_listView.hidden || !_mapView.hidden){
        //        if(!isSearching){
        if(filterArr.count){
            garObject = (GarageObject*)[filterArr objectAtIndex:btn.tag];
            [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",garObject._address,garObject._city,garObject._state] withLatitute:garObject._latitude andLongitute:garObject._longitute];
            
        }
        //        }
        //        else{
        //            if(searchArr.count){
        //                garObject = (GarageObject*)[searchArr objectAtIndex:btn.tag];
        //                [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",garObject._address,garObject._city,garObject._state] withLatitute:garObject._latitude andLongitute:garObject._longitute];
        //            }
        //        }
    }
    else if(!_favView.hidden){
        //        if(!isSearching){
        if(appDel.favGarageArr.count){
            garObject = (GarageObject*)[appDel.favGarageArr objectAtIndex:btn.tag];
            [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",garObject._address,garObject._city,garObject._state]withLatitute:garObject._latitude andLongitute:garObject._longitute];
        }
        //       }
        //        else{
        //            if(searchArr.count){
        //                garObject = (GarageObject*)[searchArr objectAtIndex:btn.tag];
        //                [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",garObject._address,garObject._city,garObject._state] withLatitute:garObject._latitude andLongitute:garObject._longitute];
        //            }
        //        }
        
    }
}

- (IBAction)favoriteTabBtnClicked:(id)sender
{
    MyFavoriteScreen* myFav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"MyFavoriteScreen"];
    [self.navigationController pushViewController:myFav animated:NO];
}



- (IBAction)selectNeighbourClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    
    switch (btn.tag) {
        case 0:{
            
            if(btn.selected){
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                //btn.selected = NO;
                filterImgVw1.hidden = YES;
                NeighbourHoodScreen* nhScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NeighbourHoodScreen"];
                [self.navigationController pushViewController:nhScreen animated:YES];
                
            }
            else{
                [btn setBackgroundColor:[UIColor colorWithRed:103/225.0 green:188/255.0 blue:69/255.0 alpha:1.0]];
                
                [filterImgVw1 setImage:[UIImage imageNamed:@"checkIcon.png"]];
                //btn.selected = YES;
                filterImgVw1.hidden = NO;
                
                NeighbourHoodScreen* nhScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NeighbourHoodScreen"];
                [self.navigationController pushViewController:nhScreen animated:YES];
                
            }
        }
            break;
            
        case 1:{
            
            if(btn.selected){
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                btn.selected = NO;
                filterImgVw2.hidden = YES;
            }
            else{
                [btn setBackgroundColor:[UIColor colorWithRed:103/225.0 green:188/255.0 blue:69/255.0 alpha:1.0]];
                [filterImgVw2 setImage:[UIImage imageNamed:@"checkIcon.png"]];
                btn.selected = YES;
                filterImgVw2.hidden = NO;
            }
        }
            break;
            
        case 2:{
            
            if(btn.selected){
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                btn.selected = NO;
                filterImgVw3.hidden = YES;
            }
            else{
                [btn setBackgroundColor:[UIColor colorWithRed:103/225.0 green:188/255.0 blue:69/255.0 alpha:1.0]];
                [filterImgVw3 setImage:[UIImage imageNamed:@"checkIcon.png"]];
                btn.selected = YES;
                filterImgVw3.hidden = NO;
            }
        }
            break;
            
        case 3:{
            
            if(btn.selected){
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                btn.selected = NO;
                filterImgVw4.hidden = YES;
            }
            else{
                [btn setBackgroundColor:[UIColor colorWithRed:103/225.0 green:188/255.0 blue:69/255.0 alpha:1.0]];
                [filterImgVw4 setImage:[UIImage imageNamed:@"checkIcon.png"]];
                btn.selected = YES;
                filterImgVw4.hidden = NO;
            }
        }
            break;
            
        case 4:{
            
            if(btn.selected){
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                btn.selected = NO;
                filterImgVw4.hidden = YES;
            }
            else{
                [btn setBackgroundColor:[UIColor colorWithRed:103/225.0 green:188/255.0 blue:69/255.0 alpha:1.0]];
                [filterImgVw5 setImage:[UIImage imageNamed:@"checkIcon.png"]];
                btn.selected = YES;
                filterImgVw5.hidden = NO;
            }
        }
            break;
            
        default:
            break;
    }
    
}


#pragma mark- ServerConnect Delegate
- (void) dataLoadRequestFailedWithError:(NSError*)error andMsg:(NSString*)errorMsg;
{
    [Utility stopLoading:self.view];
    NSLog(@"Error in ParkMap View Controller");
}
- (void) dictLoadedFromServer:(NSDictionary*)dict;
{
    // End Refreshing
    [refreshControl endRefreshing];
    //[Utility stopLoading:self.view];
    NSLog(@"Map Dict %@",dict);

    if ([[dict valueForKey:@"Items"] count]>0) {
        garageDict = dict;
    }
    else{
        if (isSearching || isdragMap) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"There are no results that match your search." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert performSelector:@selector(show) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
        }
    }
        
    [[garageDict valueForKey:@"Items"] count] > 0 ? [self storeFilterationValuesIntoModel:[dict valueForKey:@"Items"]]:NSLog(@"Did not get any item");
    
}

- (void) stringLoadedFromServer:(NSString*)str;
{
    
}
- (void) arrayLoadedFromServer:(NSArray*)Array;
{
    NSLog(@"Received array is %@",Array);
    
}

#pragma mark - Neighbourhood Name
-(void)storeFilterationValuesIntoModel:(NSArray*)array{
    NSMutableArray* neighArr = [[NSMutableArray alloc] init];
    for (NSInteger indexOne = 0; indexOne < [array count]; indexOne++) {
        for (NSInteger indexTwo = 0; indexTwo < [neighArr count]; indexTwo++) {
            if ([array[indexOne] valueForKey:@"NeighborhoodName"] != (id)[NSNull null]) {
                if ([neighArr[indexTwo]isEqualToString:[array[indexOne] valueForKey:@"NeighborhoodName"]]) {
                    [neighArr removeObjectAtIndex:indexTwo];
                }
            }
        }
        if ([array[indexOne] valueForKey:@"NeighborhoodName"] != (id)[NSNull null])
            [neighArr addObject:[array[indexOne] valueForKey:@"NeighborhoodName"]];
    }
    parkModel = [ParkNeighbourhoodModel sharedManager];
    parkModel.arrayNameFilteration = [[NSArray alloc] initWithArray:neighArr];
    parkModel.arrayItemMain = [[NSArray alloc] initWithArray:array];
}



#pragma mark- UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == dateTxtFld1 || textField == dateTxtFld2) {
        
        _datePickerView.hidden = NO;
        _rangeSlider.hidden = YES;
        _priceLbl.hidden = YES;
        _filterBtnVw.hidden = YES;
        
        //[self._scrollViewFilter bringSubviewToFront:_datePickerView];
        
        switch (textField.tag) {
            case 0:{
                [_datePickerView setFrame:CGRectMake(50,133,265,162)];
                datePckFlg = textField.tag;
            }
                break;
                
            case 1:{
                [_datePickerView setFrame:CGRectMake(50,133,265,162)];
                datePckFlg = textField.tag;
            }
                break;
                
                
            default:
                break;
        }
        
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
{
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
//{
//    if(textField.tag == 0)
//        [self textFieldDidChangeText:[textField.text stringByReplacingCharactersInRange:range withString:string]];
//
//    return YES;
//}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // NSLog(@"XTXT TAG %ld",(long)textField.tag);
    [textField resignFirstResponder];
    
    if(textField.text.length > 0){
        [parkModel.arraySelectedBtns removeAllObjects];
        isdragMap = NO;
        isSearching = YES;
        self._mkMapView.showsUserLocation = NO;
        itemArray = [[NSMutableArray alloc]init];
        searchPageCount = 0;
        [self searchBackgroundWithSearchText:textField.text];
    }
    //    else{
    //        //isSearching = NO;
    //        if(!_listView.hidden || !_mapView.hidden){
    //
    //            if(self._mkMapView.annotations)
    //                [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    //
    //            [self mapViewDetails];
    //            [_tableViewList reloadData];
    //
    //        }
    //        else if(!_favView.hidden){
    //            [_tableViewFav reloadData];
    //        }
    //    }
    return YES;
}


-(void)searchBackgroundWithSearchText:(NSString*)searchStr
{
    [Utility showLoadingIndicator:@"" withView:self.view];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        searchPageCount++;
        [self searchApiCall:searchPageCount WithSearchString:searchStr];
        // Perform the asynchronous task in this block like loading data from server
        
        // Perform the task on the main thread using the main queue-
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            // Perform the UI update in this block, like showing image.
            [Utility stopLoading:self.view];
            
            if(!_listView.hidden || !_mapView.hidden) {
                appDel.searchString = searchStr;
                if(self._mkMapView.annotations)
                    [self._mkMapView removeAnnotations:self._mkMapView.annotations];
                
                NSMutableArray* searchDummyArr = [[NSMutableArray alloc]init];
                NSArray* searchListArr;
                
                for (int i=0; i<[[garageDict valueForKey:@"Items"] count]; i++){
                    GarageObject* garObj = [GarageObject garagesFromDict:[[garageDict valueForKey:@"Items"] objectAtIndex:i]];
                    [searchDummyArr addObject:garObj];
                }
                
                searchListArr = itemArray;
                filterArr = [[NSMutableArray alloc]init];
                itemArray = [[NSMutableArray alloc]init];
                
                
                for (int i=0; i<[searchDummyArr count]; i++){
                    GarageObject* garObj = [searchDummyArr objectAtIndex:i];
                    [filterArr addObject:garObj];
                }
                
                for (int i=0; i<[searchListArr count]; i++){
                    GarageObject* garObj = [searchListArr objectAtIndex:i];
                    [filterArr addObject:garObj];
                }
                itemArray = filterArr;
                [self mapViewDetails];
                [_tableViewList reloadData];
                
            }
            else if(!_favView.hidden){
                //searchArr = [appDel.favGarageArr filteredArrayUsingPredicate:predicate];
                [_tableViewFav reloadData];
                
            }
            //
            //            if(self._mkMapView.annotations)
            //                [self._mkMapView removeAnnotations:self._mkMapView.annotations];
            //
            //            [self mapViewDetails];
            //            [_tableViewList reloadData];
        });
    });
}


-(void)textFieldDidChangeText:(NSString*)textStr
{
    if(textStr.length > 0){
        isSearching = YES;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"_name contains[c] %@", textStr];
        
        if(!_listView.hidden || !_mapView.hidden){
            //searchArr = [filterArr filteredArrayUsingPredicate:predicate];
            
            if(self._mkMapView.annotations)
                [self._mkMapView removeAnnotations:self._mkMapView.annotations];
            
            [self mapViewDetails];
            [_tableViewList reloadData];
            
        }
        else if(!_favView.hidden){
            //searchArr = [appDel.favGarageArr filteredArrayUsingPredicate:predicate];
            [_tableViewFav reloadData];
            
        }
        
    }
    else{
        isSearching = NO;
        
        if(!_listView.hidden || !_mapView.hidden){
            
            if(self._mkMapView.annotations)
                [self._mkMapView removeAnnotations:self._mkMapView.annotations];
            
            [self mapViewDetails];
            [_tableViewList reloadData];
            
        }
        else if(!_favView.hidden){
            [_tableViewFav reloadData];
        }
    }
    
}

-(void)hideNavigationView:(BOOL)navChk{
    navRightView.hidden = navChk;
    searchView.hidden = navChk;
}

#pragma mark - UIDatePicker PickerTapped

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    // return
    return true;
}
-(void)pickerTapped:(UIGestureRecognizer *)gestureRecognizer
{
    _datePickerView.hidden = YES;
    _rangeSlider.hidden = NO;
    _priceLbl.hidden = NO;
    _filterBtnVw.hidden = NO;
    
    
    [self selectedDateFromPicker];
}


-(void)selectedDateFromPicker
{
    NSDate *selected = [datePickerObj date];
	NSString *message = [[NSString alloc] initWithFormat:@"%@",selected];
    NSArray *myWords = [message componentsSeparatedByString:@" "];
    NSLog(@"%@",myWords);
    
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"EEE MM/dd/yyyy       hh:mma";
    
    //timeFormatter.dateFormat = @"EEE, LLL d, yyyy hh:mma";
    NSString* stringForNewTime = [timeFormatter stringFromDate:selected];
    NSLog(@"Date %@",stringForNewTime);
    
    switch (datePckFlg) {
        case 0:
            dateTxtFld1.text = stringForNewTime;
            break;
            
        case 1:
            dateTxtFld2.text = stringForNewTime;
            break;
            
        default:
            break;
    }
}






-(void)apiCoupon
{
    ServerConnect* servConnectObj = [[ServerConnect alloc]init];
    servConnectObj._delegate = self;
    
    NSString* uuid =  @"aa537c33-5953-45fb-a823-5f48b3378fdd";
    
    //    NSString* jsonStr = [NSString stringWithFormat:@"ProviderGUID=%@&UserName=park&Password=rwet23fwe&Locations=NewYork&CouponQuantity=1&CouponExpirationDate=12JULY2014&ProgramId=12&CouponValue=12&CouponType=1&PromotionID=HELL",uuid];
    
    
    NSMutableDictionary* couponsDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"aa537c33-5953-45fb-a823-5f48b3378fdd",@"ProviderGUID",@"park",@"UserName",@"rwet23fwe",@"Password",@"New York",@"Locations",@"1",@"CouponQuantity",@"30 July 2014",@"CouponExpirationDate",@"12",@"ProgramId",@"12",@"CouponValue",@"1",@"CouponType",@"FREE PARK",@"PromotionID", nil];
    
    NSString* jsonCoupon =[Utility convertDictionaryToString:couponsDict];
    
    [servConnectObj createConnectionRequestToURL:@"http://coupons.ticketech.com/ProviderWebService.asmx?op=GenerateCoupons" withJsonString:jsonCoupon withType:@"POST"];
}



- (IBAction)pointerButtonTapped:(UIButton *)sender
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(_lastMapCenterCoordinate, MAP_VIEW_REGION_DISTANCE, MAP_VIEW_REGION_DISTANCE);
    MKCoordinateRegion adjustedRegion = [self._mkMapView regionThatFits:region];
    [self._mkMapView setRegion:adjustedRegion animated:YES];
}



// Park-2#Changes

-(void)setRecentDataKeyIntoModel{
    parkModel.isReloadData = @"reload_data";
}

- (void)getLatestGarageDetailsWithArray:(NSArray *)array
{
    //[Utility stopLoading:self.view];
    if(self._mkMapView.annotations) {
        [self._mkMapView removeAnnotations:self._mkMapView.annotations];
    }
    
    NSMutableArray* dummyArr = [[NSMutableArray alloc]init];
    
    for (int i=0; i<array.count; i++){
        GarageObject* garObj = [GarageObject garagesFromDict:[array objectAtIndex:i]];
        [dummyArr addObject:garObj];
    }
    for (int i=0; i<[dummyArr count]; i++){
        GarageObject* garObj = [dummyArr objectAtIndex:i];
        [filterArr addObject:garObj];
    }
    
    MKMapView* currentMap;
    currentMap.delegate = self;
    
    for(int i=0;i<[filterArr count];i++)
    {
        GarageObject* garObjs = (GarageObject*)[filterArr objectAtIndex:i];
        
        //NSLog(@"Add %@ \n Lat %@ \n Long %@ \n CRate %@",garObjs._address,garObjs._latitude,garObjs._longitute,garObjs._currntRate);
        
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake(0, 0);
        
        if(garObjs._latitude != nil && garObjs._latitude != (id)[NSNull null]){
            annotationCoord.latitude =  [garObjs._latitude floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        if(garObjs._longitute != nil && garObjs._longitute != (id)[NSNull null])
        {
            annotationCoord.longitude = [garObjs._longitute floatValue];
            annotationPoint.coordinate = annotationCoord;
        }
        
        if(!_meteredView.hidden){
            currentMap = self._meteredMapView;
            [self._meteredMapView addAnnotation:annotationPoint];
        }
        else if(!_freeView.hidden){
            currentMap = self._freeMapView;
            [self._freeMapView addAnnotation:annotationPoint];
        }
        else if(!_mapView.hidden || !_listView.hidden){
            currentMap = self._mkMapView;
            [self._mkMapView addAnnotation:annotationPoint];
        }
    }
    
    if(!_meteredView.hidden)
        [self zoomIn:self._meteredMapView];
    else if(!_freeView.hidden)
        [self zoomIn:self._freeMapView];
    else if(!_listView.hidden||!_mapView.hidden)
        [self zoomIn:self._mkMapView];
    
    
    NSLog(@"Annotation Count %lu",(unsigned long)[self._mkMapView.annotations count]);
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [Utility stopLoading:self.view];
    NSLog(@"didFailWithError: %@", error);
    [[[UIAlertView alloc]
       initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    
    [manager stopUpdatingLocation];
    [self zoomIn:self._meteredMapView];
    
    // Park-2#Changes (for if condition, else was default)
    if ([parkModel.isReloadData isEqualToString:@"reload_data"])
        [self getLatestGarageDetailsWithArray:parkModel.arrayItemMain];
    else
        [self garageDetailsInBackgroundWithLat:manager.location.coordinate.latitude andLong:manager.location.coordinate.longitude];
    //[Utility stopLoading:self.view];
}


// @end



#pragma mark Delegate Methods

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    SettingsViewController* settingsViewObj = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:settingsViewObj animated:YES];
}
@end
