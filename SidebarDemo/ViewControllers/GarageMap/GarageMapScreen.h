//
//  GarageMapScreen.h
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ParkAnnotation.h"
#import <CoreLocation/CoreLocation.h>

#define SCROLL_UPDATE_DISTANCE          80.00

@interface GarageMapScreen : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate, CLLocationManagerDelegate>
{
    IBOutlet UIDatePicker* datePickerObj;
}

@property (nonatomic,retain)CLLocationManager *locationManager;



@end
