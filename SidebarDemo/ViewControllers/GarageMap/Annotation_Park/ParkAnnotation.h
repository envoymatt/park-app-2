//
//  ParkAnnotation.h
//  Park.com
//
//  Created by Mohd Rahib on 13/08/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface ParkAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic) NSUInteger locationId;

- (void)setLat:(CLLocationDegrees)lat andLongt:(CLLocationDegrees)longt;


@end
