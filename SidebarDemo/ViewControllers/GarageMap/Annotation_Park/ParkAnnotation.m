//
//  ParkAnnotation.m
//  Park.com
//
//  Created by Mohd Rahib on 13/08/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "ParkAnnotation.h"

@implementation ParkAnnotation

- (void)setLat:(CLLocationDegrees)lat andLongt:(CLLocationDegrees)longt
{
    _coordinate =  CLLocationCoordinate2DMake(lat, longt);
}

- (NSString *)title
{
    return @" ";
}

- (NSString *)subtitle
{
    return @" ";
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[ParkAnnotation class]])
    {
        ParkAnnotation *annotation = (ParkAnnotation *)object;

        if (annotation.locationId == self.locationId)
        {
            return YES;
        }
    }

    return NO;
}


@end
