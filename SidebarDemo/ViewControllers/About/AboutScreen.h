//
//  AboutScreen.h
//  Park.com
//
//  Created by Devendra Singh on 6/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface AboutScreen : UIViewController<MFMailComposeViewControllerDelegate>

@end
