//
//  AboutScreen.m
//  Park.com
//
//  Created by Devendra Singh on 6/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "AboutScreen.h"
#import "Utility.h"
#import "SWRevealViewController.h"
#import "MyCouponeScreen.h"
#import "RestaurantNameScreen.h"
#import "ParkListScreen.h"
#import "ParkNearByScreen.h"
#import "NavigationDecisionScreen.h"


@interface AboutScreen ()

//- (IBAction)myCouponesClicked:(id)sender;
//- (IBAction)restaurantClicked:(id)sender;
//- (IBAction)parkListClicked:(id)sender;
//- (IBAction)parkNearByClicked:(id)sender;
//- (IBAction)navigationClicked:(id)sender;
- (IBAction)supportBtnClicked:(id)sender;
- (IBAction)contactUsClicked:(id)sender;
@end

@implementation AboutScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Actions

- (IBAction)myCouponesClicked:(id)sender {
    MyCouponeScreen* myCoupons = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"MyCouponeScreen"];
    [self.navigationController pushViewController:myCoupons animated:YES];
}

- (IBAction)restaurantClicked:(id)sender {
    RestaurantNameScreen* restNmScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"RestaurantNameScreen"];
    [self.navigationController pushViewController:restNmScreen animated:YES];
}

- (IBAction)parkListClicked:(id)sender {
    ParkListScreen* pListScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"ParkListScreen"];
    [self.navigationController pushViewController:pListScreen animated:YES];
}

- (IBAction)parkNearByClicked:(id)sender {
    ParkNearByScreen *parkNBy = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"ParkNearByScreen"];
    [self.navigationController pushViewController:parkNBy animated:YES];
}

- (IBAction)navigationClicked:(id)sender {
    NavigationDecisionScreen* navScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationDecisionScreen"];
    [self.navigationController pushViewController:navScreen animated:YES];
}


- (IBAction)supportBtnClicked:(id)sender {
    // Email Subject
    //NSString *emailTitle = @"Test Email";
    // Email Content
    //NSString *messageBody = @"iOS programming is so fun!";
    // To address
    //NSArray *toRecipents = [NSArray arrayWithObject:@"email@address.com"];
    
    MFMailComposeViewController *mailComp = [[MFMailComposeViewController alloc] init];
    mailComp.mailComposeDelegate = self;
    [mailComp setSubject:@""];
    [mailComp setMessageBody:@"" isHTML:NO];
    [mailComp setToRecipients:[NSArray arrayWithObject:@"support@park.com"]];
    
    // Present mail view controller on screen
    [self presentViewController:mailComp animated:YES completion:NULL];
}

- (IBAction)contactUsClicked:(id)sender {
   MFMailComposeViewController* mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setToRecipients:[NSArray arrayWithObject:@"contactus@park.com"]];
    [mailComposer setSubject:@""];
    [mailComposer setMessageBody:@"" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:NULL];

}


#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
             didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
//                 if (result) {
//                     NSLog(@"Result : %d",result);
//                 }
//                 if (error) {
//                     NSLog(@"Error : %@",error);
//                 }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    //[menuButton addTarget:self action:@selector forControlEvents:UIControlEventTouchUpInside];
    
    //_sidebarButton.tintColor = [UIColor colorWithWhite:0.96f alpha:0.2f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    //_sidebarButton.target = self.revealViewController;
    //_sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    NSLog(@"%@",self.revealViewController);
    //[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"left" style:UIBarButtonItemStyleBordered target:self.viewDeckController action:@selector(toggleLeftView)];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    [searchButton setImage:[UIImage imageNamed:@"searchimage.png"] forState:UIControlStateNormal];
    
    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[navRightView addSubview:searchButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navRightView addSubview:titleSepratorImg];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"About";
    self.navigationItem.titleView = label;
    
}

@end
