//
//  MyFavoriteScreen.m
//  Park.com
//
//  Created by Devendra Singh on 6/11/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "MyFavoriteScreen.h"
#import "Utility.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "ParkListCell.h"
#import "GarageObject.h"
#import "NavigationDecisionScreen.h"
#import "GarageView.h"

@interface MyFavoriteScreen ()
{
    NSString* cellIdentifier;
    AppDelegate* appDelObj;
    NSMutableArray* favArr;
}
@property (strong, nonatomic) IBOutlet UITableView *_favTableVw;
- (IBAction)clearListClicked:(id)sender;

@end

@implementation MyFavoriteScreen
@synthesize _favTableVw;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [_favTableVw reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavigationBar];
    
    appDelObj = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    cellIdentifier = @"CellIdentifier";
    favArr = [[NSMutableArray alloc]init];
    
    
    [_favTableVw registerNib:[UINib nibWithNibName:@"ParkListCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    //[menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    
    
    UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
    [searchButton setImage:[UIImage imageNamed:@"searchimage.png"] forState:UIControlStateNormal];
    
    //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navRightView addSubview:searchButton];
    
    //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
    //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
    //[navRightView addSubview:titleSepratorImg];
    
    UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"My Favorites";
    self.navigationItem.titleView = label;
    
}




#pragma mark - UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(appDelObj.favGarageArr.count)
        return [appDelObj.favGarageArr count];
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ParkListCell *cell = (ParkListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        cell = [[ParkListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    else
        [cell prepareForReuse];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(appDelObj.favGarageArr.count){
        
        cell._detailView.hidden = NO;
        cell._noRecordsLbl.hidden = YES;
        
        GarageObject* garObj = (GarageObject*)[appDelObj.favGarageArr  objectAtIndex:indexPath.row];
        [favArr addObject:garObj];
        cell._garageNameLbl.text = garObj._name;
        cell._addressLbl.text = garObj._address;
        cell._cityStateZipLbl.text = [NSString stringWithFormat:@"%@, %@, %@",garObj._city,garObj._state,garObj._postal];
        
        cell._favBtn.tag = indexPath.row;
        cell._navigationBtn.tag = indexPath.row;
        
        [cell._favBtn addTarget:self action:@selector(favoriteBtnListViewClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell._navigationBtn addTarget:self action:@selector(navigationBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if(garObj.favKey){
            [cell._favBtn setImage:[UIImage imageNamed:@"btn-favorite1-1"] forState:UIControlStateNormal];
            cell._favBtn.selected = YES;
            garObj.favKey = YES;
        }
        else{
            [cell._favBtn setImage:[UIImage imageNamed:@"btn-favorite1-0"] forState:UIControlStateNormal];
            cell._favBtn.selected = NO;
            garObj.favKey = NO;

        }
        
        if(garObj._phone != (id)[NSNull null]){
            NSMutableString *stringts = [NSMutableString stringWithString:garObj._phone];
            [stringts insertString:@"-" atIndex:3];
            [stringts insertString:@"-" atIndex:7];
            cell._phoneNoLbl.text = stringts;
        }
        
        cell._typeLbl.text = [NSString stringWithFormat:@"Type: %@",garObj._type];
        [cell._btnCost setTitle:[NSString stringWithFormat:@"$%@",garObj._currntRate] forState:UIControlStateNormal];
        
    }
    else{
        cell._detailView.hidden = YES;
        cell._noRecordsLbl.hidden = NO;
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self navigateToListView:indexPath.row];
}

-(void)navigateToListView:(int)garageIndex
{
    GarageObject* garObjectF;
    garObjectF = (GarageObject*)[appDelObj.favGarageArr objectAtIndex:garageIndex];
    
    //    if (!isSearching) {
    //       garObject = (GarageObject*)[filterArr objectAtIndex:garageIndex];
    //    }
    //    else if (isSearching)
    //        garObject = (GarageObject*)[searchArr objectAtIndex:garageIndex];
    
    //    if (![appDel.recentVisitedArr containsObject:[filterArr objectAtIndex:garageIndex]]) {
    //        [appDel.recentVisitedArr addObject:[filterArr objectAtIndex:garageIndex]];
    //    }
    
    
    GarageView* gView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GarageView"];
    [self.navigationController pushViewController:gView animated:YES];
    [gView garageDetailId:garObjectF];
    
}

- (IBAction)clearListClicked:(id)sender {
    
    [appDelObj.favGarageArr removeAllObjects];
    [_favTableVw reloadData];
}


- (void)favoriteBtnListViewClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    NSLog(@"Button selected %d  btn tag %ld",btn.selected,(long)btn.tag);
    GarageObject* garObj = [favArr objectAtIndex:btn.tag];
    
    if (!btn.selected) {
        if (![appDelObj.favGarageArr containsObject:[favArr objectAtIndex:btn.tag]]) {
            [appDelObj.favGarageArr addObject:[favArr objectAtIndex:btn.tag]];
        }
        btn.selected = YES;
        garObj.favKey = YES;
        [btn setImage:[UIImage imageNamed:@"btn-favorite1-1"] forState:UIControlStateNormal];
    }
    else{
        if ([appDelObj.favGarageArr containsObject:[favArr objectAtIndex:btn.tag]]) {
            [appDelObj.favGarageArr removeObject:[favArr objectAtIndex:btn.tag]];
        }
        btn.selected = NO;
        garObj.favKey = NO;
        [btn setImage:[UIImage imageNamed:@"btn-favorite1-0"] forState:UIControlStateNormal];
    }
    [favArr removeAllObjects];
    NSLog(@"Arr %@",appDelObj.favGarageArr);
    [_favTableVw reloadData];
}

-(void)navigationBtnClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    GarageObject* garObj = [appDelObj.recentVisitedArr objectAtIndex:btn.tag];
    NavigationDecisionScreen* navDecPScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavigationDecisionScreen"];
    [self.navigationController pushViewController:navDecPScreen animated:YES];
    [navDecPScreen destinationAddress:[NSString stringWithFormat:@"%@ %@ %@",garObj._address,garObj._city,garObj._state]withLatitute:garObj._latitude andLongitute:garObj._longitute];
}

@end
