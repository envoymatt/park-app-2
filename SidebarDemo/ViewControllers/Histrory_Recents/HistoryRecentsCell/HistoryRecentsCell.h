//
//  HistoryRecentsCell.h
//  Park.com
//
//  Created by Devendra Singh on 6/11/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryRecentsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *_garageNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *_addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *_cityStateZipLbl;
@property (weak, nonatomic) IBOutlet UILabel *_phoneNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *_typeLbl;
@property (weak, nonatomic) IBOutlet UIButton *_btnCost;

@end
