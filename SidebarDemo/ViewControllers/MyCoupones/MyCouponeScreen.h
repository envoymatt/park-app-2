//
//  MyCouponeScreen.h
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponeScreen : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
