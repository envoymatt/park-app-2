//
//  MyCouponeScreen.m
//  Park
//
//  Created by Devendra Singh on 6/3/14.
//  Copyright (c) 2014 Comnez Inc. All rights reserved.
//

#import "MyCouponeScreen.h"
#import "CouponsCell.h"
#import "ParkFilterScreen.h"
#import "Utility.h"
#import "SWRevealViewController.h"


@interface MyCouponeScreen ()
{
    NSString* cellIdentifier;
    
    NSArray* imageArray;
}
@property (weak, nonatomic) IBOutlet UITableView *_mycoupnsTable;

@end

@implementation MyCouponeScreen
@synthesize _mycoupnsTable;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    imageArray = [NSArray arrayWithObjects:@"1.png",@"2.png",@"3.png",@"1.png",@"2.png",@"3.png",@"1.png",@"2.png",@"3.png",@"1.png",@"2.png",@"3.png", nil];
    cellIdentifier = @"CellIdentifier";
    [_mycoupnsTable registerNib:[UINib nibWithNibName:@"CouponsCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        CouponsCell *cell = (CouponsCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil)
        {
            cell = [[CouponsCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        else
            [cell prepareForReuse];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    [cell._imageHotel setImage:[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]]];
        return cell;
 
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //ParkFilterScreen* parkFltrScreen = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"ParkFilterScreen"];
    //[self.navigationController pushViewController:parkFltrScreen
    //                                     animated:YES];
}


#pragma mark - Navigation Bar
-(void)setupNavigationBar
{
    [self changeStatusBarColor];
    
    UIView * navLeftView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 29, 29)];
    UIButton* menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 29, 29)];
    [menuButton setImage:[UIImage imageNamed:@"header-menu.png"] forState:UIControlStateNormal];
    
    //[menuButton addTarget:self action:@selector(headerMenu:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [navLeftView addSubview:menuButton];
    
    UIBarButtonItem* topMenukButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftView];
    [self.navigationItem setLeftBarButtonItem:topMenukButton];
    
    
    
        UIView * navRightView= [[UIView alloc]initWithFrame:CGRectMake(276, 0, 34, 34)];
        UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchButton setFrame:CGRectMake(0, 0, 34, 34)];
        [searchButton setImage:[UIImage imageNamed:@"searchimage.png"] forState:UIControlStateNormal];
    
        //[searchButton addTarget:self action:@selector(searchBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [navRightView addSubview:searchButton];
    
        //UIImageView *titleSepratorImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title-seperator"]];
        //[titleSepratorImg setFrame:CGRectMake(50, 0, 2, 42)];
        //[navRightView addSubview:titleSepratorImg];
    
        UIBarButtonItem* topSearchButton = [[UIBarButtonItem alloc] initWithCustomView:navRightView];
    [self.navigationItem setRightBarButtonItem:topSearchButton];
    
    [self navigationViewTitle];
    
}

-(void)headerMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark StausBarColor
-(void)changeStatusBarColor
{
    UIImage *imageResize;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,64)];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    else
        imageResize = [Utility imageWithImage:[UIImage imageNamed:@"header-bg.png"] scaledToSize:CGSizeMake(320,44)];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:imageResize forBarMetrics:UIBarMetricsDefault];
    
}

-(void)navigationViewTitle
{
    CGRect frame = CGRectMake(70,0,180,44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.text = @"My Coupons";
    self.navigationItem.titleView = label;
    
}

@end
