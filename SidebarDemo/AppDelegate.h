//
//  AppDelegate.h
//  SidebarDemo
//
//  Created by Simon on 28/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Appsee/Appsee.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,retain)CLLocationManager *locationManager;

@property (strong, nonatomic) NSMutableArray* recentVisitedArr;
@property (strong, nonatomic) NSMutableArray* favGarageArr;
@property(nonatomic) BOOL isHitAPI;
@property (strong, nonatomic) NSString* searchString;
//@property (strong, nonatomic) NSString* longitute;
//@property (strong, nonatomic) UINavigationController *_navController;

@end
